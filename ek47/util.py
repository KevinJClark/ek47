import json
import math
import os
import random
import ipaddress
import socket

from .output import colorize, display
from .generate import generate_random_hex_string
from urllib.parse import urlparse


def is_output_naughty(output: str) -> bool:
    # Implement checks such that the output filename is not a DIRTY WORD
    # If you really want to bypass this, just uncomment the line below. Hack responsibly

    # return True

    naughty_words = ["sploit", "meterpreter", "msf", "metsrv", "stage",
                     "badrat", "ek47", "cobalt", "beacon", "potato",
                     "shell", "malware", "payload", "privesc", "grunt",
                     "implant", "exploit", "rubeus", "bloodhound",
                     "mimikatz", "sharp", "dump", "sliver", "empire"]

    output = os.path.basename(output).lower()
    for word in naughty_words:
        if word in output:
            return False
    return True

def calculate_shannon_entropy(data: bytes) -> float:
    """
    Calculate the Shannon entropy value of the given data.

    The Shannon entropy measures the amount of uncertainty or information contained in a set of data.
    The result will be between 0.000 and 8.000.

    Args:
        data (bytes): The input data for entropy calculation.

    Returns:
        float: The calculated Shannon entropy value.

    References:
        - Original code: https://github.com/stanislavkozlovski/python_exercises/blob/master/easy_263_calculate_shannon_entropy.py
        - Stack Overflow: https://stackoverflow.com/questions/6256437/entropy-in-binary-files-whats-the-purpose
    """
    probability = [float(data.count(c)) / len(data) for c in dict.fromkeys(list(data))]
    entropy = -sum([p * math.log(p) / math.log(2.0) for p in probability])
    return round(entropy, 3)

def convert_environmental_keys_arguments_to_dict(arguments):
    env = {}
    if arguments.json:
        try:
            env = json.loads(arguments.json)
            display(f"Adding {len(env)} items to key on from JSON data", 'INFORMATION')
        except:
            display("Invalid JSON string. (Did you put 'single quotes' around your JSON string?) Exiting!", 'ERROR')
            quit()

    if arguments.username:
        env['username'] = arguments.username.upper()
        display(f"Adding username to list of items to key on: {env['username']}", 'INFORMATION')

    if arguments.domain:
        env['userdomain'] = arguments.domain.upper()
        if "." in env['userdomain']:
            display(colorize(f"Domain name ({env['userdomain']}) should be specified as a NETBIOS-style domain name. This probably won't work! (Try \"{env['userdomain'].split('.')[0]}\" instead).", color="red", bold=True), 'WARN')
        display(f"Adding domain to list of items to key on: {env['userdomain']}", 'INFORMATION')

    if arguments.computername:
        env['computername'] = arguments.computername.upper()
        if "." in env['computername']:
            display(colorize(f"Computer name ({env['computername']}) should not be specified as an FQDN or have dots (\".\") in it. This probably won't work! (Try \"{env['computername'].split('.')[0]}\" instead).", color="red", bold=True), 'WARN')
        display(f"Adding computername to list of items to key on: {env['computername']}", 'INFORMATION')

    if arguments.guardwords:
        arguments.guardwords.reverse()
        param_str = ""
        for i, guardword in enumerate(arguments.guardwords, start=1):
            env[f'guardword{i}'] = guardword
            param_str = f"{guardword} {param_str}"
        display(f"Adding guardword(s) to list of items to key on. First argument(s) to executable must be: {param_str.strip()}", 'INFORMATION')

    if arguments.static_keys:
        if not any([arguments.username, arguments.domain, arguments.computername, arguments.guardwords, arguments.json]):
            display(colorize("Static key encryption was the only method chosen! "
                "Your payload is suceptible to recovery and analysis by defenders!", 'red', bold=True), 'WARN')
        for _ in range(0, arguments.static_keys):
            env[f'static{generate_random_hex_string()}'] = 'static'
        
    if arguments.dns_key:
        hostname = arguments.dns_key[0]
        if len(arguments.dns_key) == 1:
            display(f"Performing dns lookup for {hostname}", 'INFORMATION')
            try:
                ip_addr = socket.gethostbyname(hostname)
            except:
                display(f"Could not look up host ({hostname})'s IP!. (Maybe try manually specifying the resulting IP address?) Exiting!", 'ERROR')
                quit()
        if len(arguments.dns_key) == 2:
            ip_addr = arguments.dns_key[1]
        env[f'namelookup{hostname}'] = ip_addr
        display(f"Adding DNS key to list of items to key on. Hostname {hostname} must resolve to: {ip_addr} on target", 'INFORMATION')

    return env

def filter_arguments(arguments):
    """
    Filter and validate the command-line arguments.

    Args:
        arguments (argparse.Namespace): The parsed command-line arguments.

    Raises:
        ValueError: If conflicting or invalid arguments are detected.

    Returns:
        argparse.Namespace: The filtered and validated command-line arguments.
    """

    if not any([arguments.username, arguments.domain, arguments.computername, arguments.guardwords, arguments.json, arguments.static_keys, arguments.dns_key]):
        display(colorize("At least one of --username (-u), --domain (-d), --computername (-c), --guardwords (-g), --static-keys (-s), "
                         "--json (-j), or --dns-key must be specified.", 'red', bold=True), 'ERROR')
        return False

    if arguments.forceexecute and arguments.bypass.lower() == "none":
        display(colorize("Cannot use --force-execute (-f) option at the same time as --bypass none.", 'red', bold=True),
                'ERROR')
        return False

    if arguments.obfuscate_jscript and not (arguments.jscript or arguments.hta or arguments.msc):
        display(colorize("The --obfuscate-jscript option can only be used with JScript (--jscript), HTA (--hta), or MSC (--msc) payloads",
                         'red', bold=True), 'ERROR')
        return False

    mutually_exclusive_options = [arguments.service_exe, arguments.install_util, arguments.msbuild, arguments.clickonce,
                                  arguments.regasm, arguments.powershell, arguments.jscript, arguments.macro, arguments.hta,
                                  arguments.asp, arguments.aspx, arguments.net_config, arguments.mssql_clr_payload, arguments.msc]
    if mutually_exclusive_options.count(True) > 1:
        display(colorize(
            f"Output options are mutually exclusive! Choose only one. (--aspx, --powershell, --regasm, --jscript, etc.)",
            'red', bold=True), 'ERROR')
        return False

    if arguments.dns_key:
        # Validate IP address
        if len(arguments.dns_key) == 2:
           try:
               _ =ipaddress.ip_address(arguments.dns_key[1])
           except:
               display(colorize(
                   f"Pre-resolved IP Address given ({arguments.dns_key[1]}) is not a valid IP address.",
                   'red', bold=True), 'ERROR')
               return False
               
        # Check to make sure there is only one DNS key
        if len(arguments.dns_key) > 2:
            display(colorize(
                f"Not allowed to specify more than one DNS key. Syntax: --dns-key <domain.com> [pre-resolved-IP-address]",
                'red', bold=True), 'ERROR')
            return False

    if arguments.clickonce:
        try:
            result = urlparse(arguments.deployment_url)
            if not all([result.scheme, result.netloc]):
                display(colorize(f'`{arguments.deployment_url}` is not a valid URL!', 'red', bold=True), 'ERROR')
                return False
        except:
            display(colorize(f'`{arguments.deployment_url}` is not a valid URL!', 'red', bold=True), 'ERROR')
            return False
        if not arguments.deployment_url.endswith(".application"):
            display(colorize("Clickonce payloads' deployment URL must end with \".application\"!", 'red', bold=True),
                    'ERROR')
            return False

    return arguments

def resolve_output(arguments) -> str:
    """
    Resolve the output file path based on the provided arguments and output.

    Args:
        arguments (object): The object containing the parsed command-line arguments.

    Returns:
        str: The resolved output file path
    """

    output = arguments.output

    # No output given. Derive the name of the output file from the payload name.
    if not output:
        payloadname = os.path.basename(arguments.payload)
        payloadname_without_extension = os.path.splitext(payloadname)[0]
        return f'{payloadname_without_extension}_ek'

    # output is a directory. Derive the file name from the payload and join that with the directory path.
    if os.path.isdir(output):
        if not os.path.exists(output):
            return ''
        path = output
        payloadname = os.path.basename(arguments.payload)
        payloadname_without_extension = os.path.splitext(payloadname)[0]
        return os.path.join(path, f'{payloadname_without_extension}')

    # output is a file. Check if output path pre-requisite directory exists.
    path = os.path.dirname(output)
    if path != '' and not os.path.exists(path):
        return ''
    output = os.path.basename(output)
    output_without_extension = os.path.splitext(output)[0]
    return os.path.join(path, f'{output_without_extension}')
