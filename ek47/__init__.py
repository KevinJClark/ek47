__version__ = '2.0'
from .compiler import compiler, check_mcs_installed
from .constants import BANNER, EPILOG
from .encrypter import encrypt_payload, compress
from .output import colorize, display
from .payloads.inputs import SUPPORTED_INPUT_PAYLOAD_FORMATS
from .payloads.outputs import SUPPORTED_OUTPUT_PAYLOAD_FORMATS
from .payloads.inputs.input import CompiledDllInputFormat
from .payloads.outputs.output import OutputFormatBase
from .util import calculate_shannon_entropy, is_output_naughty, convert_environmental_keys_arguments_to_dict, filter_arguments, resolve_output

# setup banner and epilog
DEBUG = False
BANNER = colorize(BANNER.format(__version__), 'yellow', bold=True)
EXAMPLES = '\n'.join([' ' * 4 + example for payload in SUPPORTED_INPUT_PAYLOAD_FORMATS.values() for example in payload.examples])
support_encryption_options = ['computername', 'domain', 'json', 'username', 'guardwords', 'static-keys', 'dns-key']
EPILOG = EPILOG.format(colorize(', '.join([payload_format for payload_format in SUPPORTED_INPUT_PAYLOAD_FORMATS.keys()]), 'red'),
                       colorize(', '.join(support_encryption_options), 'yellow'),
                       colorize(', '.join([output_format for output_format in SUPPORTED_OUTPUT_PAYLOAD_FORMATS.keys()]), 'green'),
                       EXAMPLES
                       )
