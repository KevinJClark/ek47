from . import bof, dotnet, dinvoke_shellcode, memorymodule_unmanaged_dll, srdi_shellcode, srdi_unmanaged_dll, unmanaged_exe, noapi_shellcode

bof = bof.Bof()
dinvoke_shellcode = dinvoke_shellcode.DinvokeShellcode()
memorymodule_unmanaged_dll = memorymodule_unmanaged_dll.MemoryModuleUnmanagedDll()
dotnet = dotnet.Dotnet()
srdi_shellcode = srdi_shellcode.SrdiShellcode()
srdi_unmanaged_dll = srdi_unmanaged_dll.SrdiUnmanagedDll()
unmanaged_exe = unmanaged_exe.UnmanagedExe()
noapi_shellcode = noapi_shellcode.NoApiShellcode()

SUPPORTED_INPUT_PAYLOAD_FORMATS = {
    bof.name: bof,
    dinvoke_shellcode.name: dinvoke_shellcode,
    memorymodule_unmanaged_dll.name: memorymodule_unmanaged_dll,
    dotnet.name: dotnet,
    srdi_shellcode.name: srdi_shellcode,
    srdi_unmanaged_dll.name: srdi_unmanaged_dll,
    unmanaged_exe.name: unmanaged_exe,
    noapi_shellcode.name: noapi_shellcode,
}
