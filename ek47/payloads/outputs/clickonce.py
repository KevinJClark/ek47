import os
import zipfile
import ek47

from . import output
from ek47.output import colorize, display


class ClickOnce(output.Ek47EmbeddedFormat):

    def __init__(self):
        super().__init__('clickonce', 'The clickonce output format.')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.exe'
        if not compiler(rendered_template_file, output_file, [], sign=arguments.strong_name, dotnet_ver=arguments.dotnet_version):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        return output_file, True

    def embed_ek47(self, arguments, outfile):
        # Generates a ZIP file an operator can use to create a clickonce application from an Ek47 payload
        short_filename, _ = os.path.splitext(outfile)
        shell_script = f"""
        REM Open a visual studio command prompt then run this script
        @echo off
        echo Running Mage.exe to compile {short_filename}.exe into {short_filename}.application...
        mkdir ClickOnce
        mkdir ClickOnce\\1.0.0.0
        move {short_filename}.exe ClickOnce\\1.0.0.0\\
        cd ClickOnce\\1.0.0.0
        mage -New Application -Processor msil -ToFile {short_filename}.manifest -name "{short_filename}" -Version 1.0.0.0 -FromDirectory .
        cd ..
        mage -New Deployment -Processor msil -Install false -Publisher "Unknown Publisher" -ProviderUrl "{arguments.deployment_url}" -AppManifest 1.0.0.0\\{short_filename}.manifest -ToFile {short_filename}.application
        echo Clickonce (.application) file should have been generated! (ClickOnce\\{short_filename}.application)
        """
        with open(outfile, 'rb') as fd:
            payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)

        clickonce_payload_name = f'{os.path.splitext(outfile)[0]}.zip'
        with zipfile.ZipFile(clickonce_payload_name, 'w') as fdzip:
            fdzip.writestr(short_filename + '.exe', payload)
            fdzip.writestr("build_clickonce.bat", shell_script)
        display(
            f"ClickOnce zip file created! Copy {clickonce_payload_name} to a Windows box, unzip it, then run the batch script from a Visual Studio command prompt to create the ClickOnce application.",
            'SUCCESS')
        return clickonce_payload_name
