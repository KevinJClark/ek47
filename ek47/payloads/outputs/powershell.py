import base64
import os
import ek47

from . import output
from ek47.output import colorize, display


class PowerShell(output.Ek47EmbeddedFormat):

    def __init__(self):
        super().__init__('powershell', 'The PowerShell output format.')

    def embed_ek47(self, arguments, outfile):
        template_name = 'powershell.ps1.template'
        display(f'Reading `{outfile}` to embed into `{template_name}`.',
                'INFORMATION')
        with open(outfile, 'rb') as fd:
            ek47_payload = fd.read()
        display(f'Removing `{outfile}` since we no longer need it.', debug=ek47.DEBUG)
        os.remove(outfile)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            assembly=base64.b64encode(ek47_payload).decode()[4:]
        )
        powershell_payload_name = f'{os.path.splitext(outfile)[0]}.ps1'
        with open(powershell_payload_name, 'w') as fd:
            fd.write(template)
        if(powershell_payload_name[0] != '/'):
            powershell_payload_name = "./" + powershell_payload_name
        display(f'PowerShell payload created! Run with: powershell.exe {powershell_payload_name}', 'SUCCESS')
        return powershell_payload_name
