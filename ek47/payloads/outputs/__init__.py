from . import asp, aspx, clickonce, dotnet, install_util, msbuild, regasm, service_exe, powershell, jscript, macro, hta, net_config, mssql_clr_payload, msc


clickonce = clickonce.ClickOnce()
dotnet = dotnet.DotNet()
install_util = install_util.InstallUtil()
msbuild = msbuild.MSBuild()
regasm = regasm.Regasm()
service_exe = service_exe.ServiceExe()
powershell = powershell.PowerShell()
jscript = jscript.JScript()
macro = macro.Macro()
hta = hta.HTA()
aspx = aspx.ASPX()
asp = asp.ASP()
net_config = net_config.NetConfig()
mssql_clr_payload = mssql_clr_payload.MssqlClrPayload()
msc = msc.MSC()

SUPPORTED_OUTPUT_PAYLOAD_FORMATS = {
    asp.name: asp,
    aspx.name: aspx,
    clickonce.name: clickonce,
    dotnet.name: dotnet,
    install_util.name: install_util,
    msbuild.name: msbuild,
    regasm.name: regasm,
    service_exe.name: service_exe,
    powershell.name: powershell,
    jscript.name: jscript,
    macro.name: macro,
    hta.name: hta,
    net_config.name: net_config,
    mssql_clr_payload.name: mssql_clr_payload,
    msc.name: msc
}
