import hashlib
import gzip

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from .output import colorize, display
from .generate import generate_string_identifier

def decrypt(key: bytes, ciphertext: bytes) -> bytes:
    # Note that the first AES.block_size bytes of the ciphertext
    # contain the IV
    iv = ciphertext[:16]
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = unpad(cipher.decrypt(ciphertext[16:]), AES.block_size)
    return msg

def encrypt(key: bytes, plaintext: bytes) -> bytes:
    # Encrypt the plaintext bytes with a provided key.
    # Generate a new 16 byte IV and include that
    # at the begining of the ciphertext
    iv = Random.new().read(AES.block_size)
    cipher = AES.new(key, AES.MODE_CBC, iv)
    msg = cipher.encrypt(pad(plaintext, AES.block_size))
    return iv + msg

def hash(hash_input: bytes) -> bytes:
    # Hashes an input 1234567 times (a lot).
    # I am no cryptoanalyist but I think this many rounds
    # of hashing should slow down brute-force attempts to
    # recover (crack) the encrypted payload.
    for i in range(0, 1234567):
        hasher = hashlib.sha256()
        hasher.update(hash_input)
        hash_input = hasher.digest()
    return hasher.digest()

def compress(decompressed_data: bytes) -> (bytes, str):
    # Compress data using gzip
    # Also measure percent reduction in size
    uncompressed_size = len(decompressed_data)
    compressed_data = gzip.compress(decompressed_data)
    compressed_size = len(compressed_data)
    compression_ratio = (1 - compressed_size / uncompressed_size) * 100
    compression_ratio = f"{compression_ratio:.1f}%"
    return compressed_data, compression_ratio

def encrypt_payload(environment: dict, assembly_bytes: bytes, payload_name: str = "an untitled payload") -> (list, bytes):
    """
    Encrypts the given .NET assembly bytes using a round of encryption for each key in the environment dictionary.

    arguments:
        environment (dict): A dictionary containing environment keys and their corresponding values.
        assembly_bytes (bytes): The .NET assembly bytes to be encrypted.
        payload_name (str, optional): Name of the payload being encrypted. Defaults to "the payload".

    Returns:
        tuple: A tuple containing a list of keys in the order of encryption and the encrypted .NET assembly bytes.
    """
    encrypted_assembly_bytes = assembly_bytes
    for index, (key, val) in enumerate(environment.items(), start=1):
        if val == 'static':
            static_key = key.replace("static", "")
            hash_val = hash(static_key.encode('utf-8'))
            display(f"Round {index} of encryption on {payload_name} with {key} -> {static_key} ({hash_val.hex()})",
                    'INFORMATION')
            encrypted_assembly_bytes = encrypt(hash_val, encrypted_assembly_bytes)
            continue
        val = val.upper().strip(',')  # Uppercase (to preserve case insensitivity) and strip comma characters
        hash_val = hash(val.encode('utf-8'))
        display(f"Round {index} of encryption on {payload_name} with {key} -> {val} ({hash_val.hex()})", 'INFORMATION')
        encrypted_assembly_bytes = encrypt(hash_val, encrypted_assembly_bytes)
    return list(environment.keys()), encrypted_assembly_bytes
