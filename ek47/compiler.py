import os
import shutil
import subprocess
import ek47

from ek47 import its
from ek47.output import colorize, display
from ek47.generate import generate_string_identifier, generate_snk_file

def check_mcs_installed() -> bool:
    """
    Check if the Mono C# Compiler (`mcs`) is already installed.

    This function attempts to run the `mcs --version` command to check if the Mono C# Compiler is installed
    on the system. It captures the output and checks for errors to determine if `mcs` is installed or not.

    Returns:
        bool: True if `mcs` is installed, False otherwise.
    """
    try:
        subprocess.run(['mcs', '--version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
        return True
    except Exception as e:
        return False

def compiler(input_file: str, output_file: str, compiler_arguments: list, sign: bool = False, unsafe: bool = False, dotnet_ver: str = "4.5") -> bool:
    # Return the compiler status as well as the public key token (if used)
    if not check_mcs_installed():
        display(f'Compiler called but MCS is not installed skipping compilation.', 'INFORMATION', debug=ek47.DEBUG)
        return False

    # '{mcs} -w:1 -out:{out} -unsafe{unsafe} -target:{target} {reference} {sign} -debug- -optimize+ {cs_file}')
    unsafe = '+' if unsafe else '-'
    debug  = '-' if ek47.DEBUG else '+'

    if sign:
        snk_file = f'{os.getcwd()}/{generate_string_identifier()}.snk'
        ek47.pub_key_token = generate_snk_file(snk_file)
        sign = f"-keyfile:{snk_file}"
        display(f"Signing assembly with SNK file: {snk_file}. Public key token: {ek47.pub_key_token}. Verify signature with `sn -v {output_file}` and `sn -T {output_file}`",
                'INFORMATION')
    else:
        sign = ''

    compiler_arguments = [ 
        shutil.which('mcs'),
        '-w:1', 
        f'-debug{debug}', 
        '-optimize+', 
        f'-unsafe{unsafe}', 
        f'-sdk:{dotnet_ver}',
        sign,
        *compiler_arguments, 
        f'-out:{output_file}', 
        input_file
    ]
    display(f'Attempting to compile `{input_file}`: {" ".join(compiler_arguments)}', debug=ek47.DEBUG)
    try:
        subprocess.run(compiler_arguments, stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    except subprocess.CalledProcessError as error:
        for error_line in error.stderr.decode('utf-8').split('\n'):
            if not error_line:
                continue
            display(colorize(error_line, 'red', bold=True), 'ERROR')
        return False

    display(f'Successfully compiled `{input_file}` to `{output_file}`.', 'SUCCESS')
    if sign:
        display(f'Removing {snk_file} as we no longer need it.', debug=ek47.DEBUG)
        os.remove(snk_file)
    return True
