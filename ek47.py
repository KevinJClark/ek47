import argparse
import ek47
import os
import random
import sys


def main():
    parser = argparse.ArgumentParser(description=ek47.BANNER, formatter_class=argparse.RawTextHelpFormatter,
                                     usage=argparse.SUPPRESS)

    # setup payload parsers
    parser._positionals.title = 'payload input formats'
    subparser = parser.add_subparsers(dest='input_format', required=True)
    for input_payload_format in ek47.SUPPORTED_INPUT_PAYLOAD_FORMATS.values():
        input_payload_format.setup_parser(subparser)

    # finalize the arguments
    parser.epilog = ek47.EPILOG
    if len(sys.argv) == 1:
        parser.print_help()
        return
    arguments = parser.parse_args()
    ek47.DEBUG = arguments.debug
    resolved_output = ek47.resolve_output(arguments)
    if not resolved_output:
        ek47.display(ek47.colorize(f'Cannot write payload to output file: {arguments.output}', 'red', bold=True), 'ERROR')
        return
    arguments.output = resolved_output
    arguments.service_exe = True if arguments.servicename else False
    arguments.clickonce = True if arguments.deployment_url else False
    arguments = ek47.filter_arguments(arguments)
    if not arguments:
        return
    ek47.display(ek47.BANNER, 'DEFAULT')

    # read in the payload
    if not ek47.is_output_naughty(arguments.output):
        ek47.display(ek47.colorize(
            f"You are hereby found guilty of generating a payload with a naughty name. Your payload was named: {arguments.output}",
            'red', bold=True), 'ERROR')
        ek47.display(ek47.colorize(f"Your punishment for this crime is ", 'red', bold=True) + ek47.colorize(
            f"{random.randint(5, 25)} lashes! ", 'white', bold=True) + ek47.colorize(
            f"Please lash yourself at your earliest convenience.", 'red', bold=True), 'ERROR')
        ek47.display(ek47.colorize(f"Tip: Use the \"--output\" flag to change the output payload name.", 'white', bold=True),
                'ERROR')
        return
    try:
        with open(arguments.payload, 'rb') as fd:
            input_payload = fd.read()
    except FileNotFoundError:
        ek47.display(ek47.colorize(f'File not found: {arguments.payload}', 'red'), 'ERROR')
        return
    except PermissionError:
        ek47.display(ek47.colorize(f'Permission denied to read file: {arguments.payload}.', 'red'), 'ERROR')
        return
    except OSError as e:
        ek47.display(ek47.colorize(f'Error reading file: {arguments.payload} - {str(e)}.', 'red'), 'ERROR')
        return

    ek47.display(ek47.colorize('Selecting input and output payload formats.', 'yellow', bold=True), 'STATUS')
    # retrieve input payload format
    try:
        input_payload_format = ek47.SUPPORTED_INPUT_PAYLOAD_FORMATS[arguments.input_format]
        ek47.display(f"Selected `{ek47.colorize(input_payload_format, 'white', bold=True)}` input payload format.")
    except KeyError:
        ek47.display(ek47.colorize(f'The input payload format `{arguments.input_format}` is not supported.', 'red'),
                     'ERROR')
        return

    # retrieve output payload format ToDo: To change when output formats are consolidated into a subparser
    if arguments.service_exe:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('service-exe')
    elif arguments.install_util:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('install-util')
    elif arguments.regasm:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('regasm')
    elif arguments.msbuild:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('msbuild')
    elif arguments.clickonce:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('clickonce')
    elif arguments.powershell:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('powershell')
    elif arguments.jscript:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('jscript')
    elif arguments.macro:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('macro')
    elif arguments.hta:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('hta')
    elif arguments.aspx:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('aspx')
    elif arguments.asp:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('asp')
    elif arguments.net_config:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('net-config')
    elif arguments.mssql_clr_payload:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('mssql-clr-payload')
    elif arguments.msc:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('msc')
    else:
        output_payload_format = ek47.SUPPORTED_OUTPUT_PAYLOAD_FORMATS.get('dotnet')
    ek47.display(f"Selected `{ek47.colorize(output_payload_format, 'white', bold=True)}` output payload format (Framework v{arguments.dotnet_version}).")

    # initialize compiler ToDo: Automated Installation, Also should be able to cancel final compliation on compiler required input formats.
    compiler_required = isinstance(input_payload_format, ek47.CompiledDllInputFormat)
    mcs_is_installed = ek47.check_mcs_installed()
    compiler_enabled = arguments.compile
    if compiler_required:
        ek47.display(ek47.colorize('Initializing mono for compiler required formats.', 'yellow', bold=True), 'STATUS')
        if not compiler_enabled:
            ek47.display(ek47.colorize(
                f'The input payload format `{arguments.input_format}` requires the compiler. Please remove '
                f'--no-compiler to continue.',
                'red', bold=True), 'ERROR')
            return
        if not mcs_is_installed:
            ek47.display(
                ek47.colorize('Compiler required and not installed, please install `mcs`.', 'white', bold=True),
                'ERROR')
            return
        ek47.display(f'Compiler required and is installed.', 'INFORMATION')
    if not compiler_enabled:
        ek47.compiler = None

    # generate the payload to be encrypted and embedded
    ek47.display(
        ek47.colorize(f'Formatting payload to the `{input_payload_format}` payload input format.', 'yellow', bold=True),
        'STATUS')
    ek47.display(f'Read in {len(input_payload)} bytes from payload file: `{arguments.payload}`', 'INFORMATION')
    ek47.display(f'Original entropy value: {ek47.calculate_shannon_entropy(input_payload)} / 8.000',
                 'INFORMATION')
    ek47.display(f'Attempting to format the input payload into the `{input_payload_format}` input payload format.',
                 debug=ek47.DEBUG)
    if compiler_required:
        payload = input_payload_format.generate_payload(arguments, input_payload, ek47.compiler)
    else:
        payload = input_payload_format.generate_payload(input_payload)
    if not payload:
        ek47.display(ek47.colorize(f'Failed to format input payload into the `{input_payload_format}` '
                                   f'input payload format.', 'red', bold=True),
                     'ERROR')
        return
    ek47.display(f'Successfully formatted input payload into the `{input_payload_format}` input payload format.',
                 'SUCCESS', debug=ek47.DEBUG)

    # compress payload if requested
    if arguments.compress:
        payload, size_reduction = ek47.compress(payload)
        ek47.display(f'Successfully compressed the input payload. Reduced payload size by: {size_reduction}. (Compressed size: {len(payload)})', 'SUCCESS')

    # encrypt payload and bypass
    ek47.display(ek47.colorize('Encrypting formatted input payload.', 'yellow', bold=True), 'STATUS')
    environmental_keys_dict = ek47.convert_environmental_keys_arguments_to_dict(arguments)  # move from util
    ek47.display(f'Attempting encrypt the formatted input payload.', debug=ek47.DEBUG)
    environmental_keys, encrypted_payload = ek47.encrypt_payload(environmental_keys_dict, payload,
                                                                 payload_name=arguments.output)
    ek47.display(f'Successfully encrypted the formatted input payload.', 'SUCCESS')
    encrypted_bypass = b''
    if arguments.bypass.lower() == 'none':
        ek47.display(f'Bypass set to `none`, skipping.')
    else:
        try:
            with open(arguments.bypass, 'rb') as fd:
                bypass = fd.read()
            ek47.display(f'Attempting encrypt the bypass `{arguments.bypass}`.', debug=ek47.DEBUG)
            _, encrypted_bypass = ek47.encrypt_payload(environmental_keys_dict, bypass, payload_name=arguments.bypass)
            ek47.display(f'Successfully encrypted the bypass `{arguments.bypass}`.', 'SUCCESS')
        except:  # ToDo: add more exceptions
            ek47.display(f"Invalid bypass path provided: {arguments.bypass}. Exiting!", 'ERROR')
            return

    # generate final payload
    ek47.display(
        ek47.colorize(f'Embedding encrypted formatted input payload into the {output_payload_format} payload output '
                      f'format.', 'yellow',
                      bold=True), 'STATUS')
    output_payload_format.generate_payload(arguments, ek47.compiler, encrypted_payload, encrypted_bypass,
                                           environmental_keys, input_payload_format.name)

if __name__ == '__main__':
    sys.exit(main())
